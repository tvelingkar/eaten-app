package com.app.eaten.selection;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.app.eaten.CommonActivity;
import com.app.eaten.R;
import com.app.eaten.util.Util;

public class SelectionActivity extends CommonActivity {

    View eateryView, customerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        this.getWindow().getDecorView().
                setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_selection);

        setupViews();
    }

    private void setupViews() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        eateryView = findViewById(R.id.eateryView);
        customerView = findViewById(R.id.customerView);

        eateryView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.getPrefs(SelectionActivity.this).edit().putInt(SELECTION_VALUE, EATERY_SELECTION).commit();
                SelectionActivity.this.setResult(RESULT_OK);
                finish();
            }
        });

        customerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.getPrefs(SelectionActivity.this).edit().putInt(SELECTION_VALUE, CUSTOMER_SELECTION).commit();
                SelectionActivity.this.setResult(RESULT_OK);
                finish();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        SelectionActivity.this.setResult(ON_BACK_PRESSED);
        finish();
    }
}
