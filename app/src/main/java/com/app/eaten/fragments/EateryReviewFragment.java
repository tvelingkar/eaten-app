package com.app.eaten.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.eaten.CommonActivity;
import com.app.eaten.R;
import com.app.eaten.adapter.CommonAdapter;
import com.app.eaten.adapter.PopulationListener;
import com.app.eaten.beans.ReviewItem;
import com.app.eaten.util.EatenConstants;
import com.app.eaten.util.ReviewItemList;
import com.app.eaten.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.techery.properratingbar.ProperRatingBar;

/**
 * Created by Tushar Velingkar on 4/12/15.
 */
public class EateryReviewFragment extends Fragment implements EatenConstants {

    private View viewEateryReview;
    private TextView emptyMessage;
    private TextView writeReview;
    private ProperRatingBar ratingBar;
    private Button addReview;
    private ListView reviewData;
    private CommonAdapter<ReviewItem> commonAdapter;

    public EateryReviewFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewEateryReview = inflater.inflate(R.layout.fragment_eatery_review, container, false);
        setupViews();
        if (Util.getPrefs(getActivity()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
            setupOwnerViews();
        } else if (Util.getPrefs(getActivity()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == CUSTOMER_SELECTION) {
            setupCustomerViews();
        }
        return viewEateryReview;
    }

    private void setupViews() {
        emptyMessage = (TextView) viewEateryReview.findViewById(R.id.empty_message);
        writeReview = (TextView) viewEateryReview.findViewById(R.id.write_review);
        ratingBar = (ProperRatingBar) viewEateryReview.findViewById(R.id.rating_bar);
        addReview = (Button) viewEateryReview.findViewById(R.id.add_review);
        reviewData = (ListView) viewEateryReview.findViewById(R.id.review_data);
    }

    private void setupOwnerViews() {
        emptyMessage.setText("No Review written yet");
        writeReview.setText("Write a Comment");
        addReview.setText("Add Comment");
        addReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReviewItem reviewItem = new ReviewItem();
                reviewItem.reviewId = 3;
                reviewItem.reviewContent = writeReview.getText().toString();
                reviewItem.reviewDate = DateFormat.getDateInstance(DateFormat.SHORT).format(new Date());// new SimpleDateFormat("dd/MM/yyyy").format(new Date());
                reviewItem.reviewRating = ratingBar.getRating();
                reviewItem.userId = Util.getPrefs(getContext()).getString(LOGIN_UNIQUE_ID, "");

                String url = SERVER_BASE_URL + POST_OWNER_COMMENT;
                postComment(reviewItem, url);
            }
        });
        if (ReviewItemList.getCount() > 0) {
            emptyMessage.setVisibility(View.GONE);
        } else {
            viewEateryReview.findViewById(R.id.button_container).setVisibility(View.GONE);
        }
        commonAdapter = new CommonAdapter<>(getActivity(), ReviewItemList.fetchAllEntries());
        int[] viewIDs = {R.id.review_user, R.id.review_rating, R.id.review_date, R.id.review_content};
        commonAdapter.setLayoutTextViews(R.layout.item_review, viewIDs);
        commonAdapter.setPopulationListener(new PopulationListener<ReviewItem>() {
            @Override
            public void populateFrom(View v, int position, ReviewItem row, View[] views) {
                ((ProperRatingBar) views[1]).setRating(row.reviewRating);
                ((TextView) views[2]).setText(row.reviewDate);
                ((TextView) views[3]).setText(row.reviewContent);
            }

            @Override
            public void onRowCreate(View[] views) {

            }
        });
        reviewData.setAdapter(commonAdapter);

    }

    private void setupCustomerViews() {
        emptyMessage.setText("Be the First to write a review on this Eatery");
        writeReview.setText("Write a Review");
        addReview.setText("Add Review");
        addReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReviewItem reviewItem = new ReviewItem();
                reviewItem.reviewContent = writeReview.getText().toString();
                reviewItem.reviewDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
                reviewItem.reviewRating = ratingBar.getRating();
                reviewItem.userId = Util.getPrefs(getContext()).getString(LOGIN_UNIQUE_ID, "");

                String url = SERVER_BASE_URL + POST_USER_REVIEW;
                postComment(reviewItem, url);
            }
        });
        if (ReviewItemList.getCount() > 0) {
            emptyMessage.setVisibility(View.GONE);
        }
        commonAdapter = new CommonAdapter<>(getActivity(), ReviewItemList.fetchAllEntries());
        int[] viewIDs = {R.id.review_user, R.id.review_rating, R.id.review_date, R.id.review_content};
        commonAdapter.setLayoutTextViews(R.layout.item_review, viewIDs);
        commonAdapter.setPopulationListener(new PopulationListener<ReviewItem>() {
            @Override
            public void populateFrom(View v, int position, ReviewItem row, View[] views) {
                ((ProperRatingBar) views[1]).setRating(row.reviewRating);
                ((TextView) views[2]).setText(row.reviewDate);
                ((TextView) views[3]).setText(row.reviewContent);
            }

            @Override
            public void onRowCreate(View[] views) {

            }
        });
        reviewData.setAdapter(commonAdapter);
    }

    public void postComment(final ReviewItem reviewItem, String url) {

        JSONObject commentObject = new JSONObject();

        JSONObject request = new JSONObject();
        try {

            request.put("eateryId", Util.getPrefs(getActivity()).getString(EATERY_ID, ""));
            request.put("reviewId", reviewItem.reviewId);
            request.put("rating", reviewItem.reviewRating);
            if (Util.getPrefs(getActivity()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
                request.put("comment", reviewItem.reviewContent);
            } else {
                request.put("review", reviewItem.reviewContent);
            }
            request.put("uniqueId", Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, ""));
            commentObject.putOpt("data", request);
            Log.v("Eaten", "Review Request "+commentObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest saveEateryRequest = new JsonObjectRequest(Request.Method.POST, url, commentObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("TAG", "response --- " + response.toString());
                Toast.makeText(getContext(), "Review has been added", Toast.LENGTH_LONG).show();
                ReviewItemList.addEntry(reviewItem);
                emptyMessage.setVisibility(View.GONE);
                commonAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Error while posting!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        ((CommonActivity) getParentFragment().getActivity()).addRequestToQueue(saveEateryRequest);
    }
}
