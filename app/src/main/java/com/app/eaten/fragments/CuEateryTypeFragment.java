package com.app.eaten.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.app.eaten.R;
import com.app.eaten.beans.EateryObject;
import com.app.eaten.beans.EateryTime;
import com.app.eaten.constants.AppConstant;
import com.app.eaten.eventbus.EventBusListener;
import com.app.eaten.util.EatenConstants;
import com.app.eaten.util.EateryTimeList;
import com.app.eaten.util.MultiSpinner;
import com.app.eaten.util.Util;
import com.google.gson.Gson;

import de.greenrobot.event.EventBus;

/**
 * Created by Tushar Velingkar on 29/11/15.
 */
public class CuEateryTypeFragment extends Fragment implements EatenConstants {

    private View cuEateryTypeView;

    TextView costForTwo;
    SwitchCompat deliveryToggle, cateringAvailableToggle;
    Button monBFBtn, monLunchBtn, monDinnerBtn, tueBFBtn, tueLunchBtn, tueDinnerBtn, wedBFBtn, wedLunchBtn, wedDinnerBtn,
            thuBFBtn, thuLunchBtn, thuDinnerBtn, friBFBtn, friLunchBtn, friDinnerBtn, satBFBtn, satLunchBtn, satDinnerBtn,
            sunBFBtn, sunLunchBtn, sunDinnerBtn;
    EditText chefNameEdit, aboutUsEdit;
    private MultiSpinner paymentTypeTxt, eateryTypeTxt, cuisineTypeTxt;
    
    public CuEateryTypeFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        cuEateryTypeView = inflater.inflate(R.layout.fragment_cu_eatery_type, container, false);
        setupCustomerViews();
        registerForEventBus();
        setupUIfromLocal();
        return cuEateryTypeView;
    }

    private void setupCustomerViews() {
        eateryTypeTxt = (MultiSpinner) cuEateryTypeView.findViewById(R.id.eateryTypeTxt);
        cuisineTypeTxt = (MultiSpinner) cuEateryTypeView.findViewById(R.id.cuisineTypeTxt);
        deliveryToggle = (SwitchCompat) cuEateryTypeView.findViewById(R.id.deliveryToggle);
        paymentTypeTxt = (MultiSpinner) cuEateryTypeView.findViewById(R.id.paymentTypeTxt);
        cateringAvailableToggle = (SwitchCompat) cuEateryTypeView.findViewById(R.id.cateringAvailabilityToggle);
        chefNameEdit = (EditText) cuEateryTypeView.findViewById(R.id.chefNameEdit);
        aboutUsEdit = (EditText) cuEateryTypeView.findViewById(R.id.aboutEdit);

        monBFBtn = (Button) cuEateryTypeView.findViewById(R.id.mondayBFBtn);
        monLunchBtn = (Button) cuEateryTypeView.findViewById(R.id.mondayLunchBtn);
        monDinnerBtn = (Button) cuEateryTypeView.findViewById(R.id.mondayDinnerBtn);
        tueBFBtn = (Button) cuEateryTypeView.findViewById(R.id.tuesdayBFBtn);
        tueLunchBtn = (Button) cuEateryTypeView.findViewById(R.id.tuesdayLunchBtn);
        tueDinnerBtn = (Button) cuEateryTypeView.findViewById(R.id.tuesdayDinnerBtn);
        wedBFBtn = (Button) cuEateryTypeView.findViewById(R.id.wedBFBtn);
        wedLunchBtn = (Button) cuEateryTypeView.findViewById(R.id.wedLunchBtn);
        wedDinnerBtn = (Button) cuEateryTypeView.findViewById(R.id.wedDinnerBtn);
        thuBFBtn = (Button) cuEateryTypeView.findViewById(R.id.thursdayBFBtn);
        thuLunchBtn = (Button) cuEateryTypeView.findViewById(R.id.thursdayLunchBtn);
        thuDinnerBtn = (Button) cuEateryTypeView.findViewById(R.id.thursdayDinnerBtn);
        friBFBtn = (Button) cuEateryTypeView.findViewById(R.id.fridayBFBtn);
        friLunchBtn = (Button) cuEateryTypeView.findViewById(R.id.fridayLunchBtn);
        friDinnerBtn = (Button) cuEateryTypeView.findViewById(R.id.fridayDinnerBtn);
        satBFBtn = (Button) cuEateryTypeView.findViewById(R.id.saturdayBFBtn);
        satLunchBtn = (Button) cuEateryTypeView.findViewById(R.id.saturdayLunchBtn);
        satDinnerBtn = (Button) cuEateryTypeView.findViewById(R.id.saturdayDinnerBtn);
        sunBFBtn = (Button) cuEateryTypeView.findViewById(R.id.sundayBFBtn);
        sunLunchBtn = (Button) cuEateryTypeView.findViewById(R.id.sundayLunchBtn);
        sunDinnerBtn = (Button) cuEateryTypeView.findViewById(R.id.sundayDinnerBtn);
        costForTwo = (TextView) cuEateryTypeView.findViewById(R.id.costForTwo);
        TextView costCurrency = (TextView) cuEateryTypeView.findViewById(R.id.costForTwoText);
        costCurrency.setText(String.format(costCurrency.getText().toString(), Util.getPrefs(getContext()).getString(COUNTRY_CURRENCY, "")));
    }

    private void registerForEventBus() {

        try {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);

        } catch (ClassCastException e) {
            e.printStackTrace();
        }

    }

    private void setupUIfromLocal() {

        Gson gson = new Gson();
        String json = Util.getPrefs(getContext()).getString(EATERY_OBJECT, "");
        EateryObject obj = gson.fromJson(json, EateryObject.class);
        if (obj != null) {
            eateryTypeTxt.setText(obj.getEateryType());
            cuisineTypeTxt.setText(obj.getCuisineType());
            paymentTypeTxt.setText(obj.getPaymentType());

            if (obj.isDeliveryAvailable()) {
                deliveryToggle.setChecked(true);
            }

            if (obj.isCateringAvailable()) {
                cateringAvailableToggle.setChecked(true);
            }

            chefNameEdit.setText(obj.getChefName());
            aboutUsEdit.setText(obj.getAboutEatery());
            costForTwo.setText(String.valueOf(obj.getCostForTwo()));
        }

        int size = EateryTimeList.fetchAllEntries().size();
        int i = 0;
        EateryTime eateryTime;
        for (; i < size; i++) {
            eateryTime = EateryTimeList.fetchAllEntries().valueAt(i);
            if (i == 0) {
                monBFBtn.setText(eateryTime.breakfast);
                monLunchBtn.setText(eateryTime.lunch);
                monDinnerBtn.setText(eateryTime.dinner);
            } else if (i == 1) {
                tueBFBtn.setText(eateryTime.breakfast);
                tueLunchBtn.setText(eateryTime.lunch);
                tueDinnerBtn.setText(eateryTime.dinner);
            } else if (i == 2) {
                wedBFBtn.setText(eateryTime.breakfast);
                wedLunchBtn.setText(eateryTime.lunch);
                wedDinnerBtn.setText(eateryTime.dinner);
            } else if (i == 3) {
                thuBFBtn.setText(eateryTime.breakfast);
                thuLunchBtn.setText(eateryTime.lunch);
                thuDinnerBtn.setText(eateryTime.dinner);
            } else if (i == 4) {
                friBFBtn.setText(eateryTime.breakfast);
                friLunchBtn.setText(eateryTime.lunch);
                friDinnerBtn.setText(eateryTime.dinner);
            } else if (i == 5) {
                satBFBtn.setText(eateryTime.breakfast);
                satLunchBtn.setText(eateryTime.lunch);
                satDinnerBtn.setText(eateryTime.dinner);
            } else if (i == 6) {
                sunBFBtn.setText(eateryTime.breakfast);
                sunLunchBtn.setText(eateryTime.lunch);
                sunDinnerBtn.setText(eateryTime.dinner);
            }
        }
    }

    public void onEvent(EventBusListener mEventListener) {
        switch (mEventListener.getType()) {
            case AppConstant.EVENT_EATERY_DATA_FETCHED:
                setupUIfromLocal();
                break;
        }
    }
}
