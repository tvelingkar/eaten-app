package com.app.eaten.customer;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.eaten.CommonActivity;
import com.app.eaten.R;
import com.app.eaten.adapter.CommonAdapter;
import com.app.eaten.adapter.FilterWithSpaceAdapter;
import com.app.eaten.adapter.PopulationListener;
import com.app.eaten.eatery.EateryFragment;
import com.app.eaten.home.HomeActivity;
import com.app.eaten.services.PlaceJSONParser;
import com.app.eaten.util.EatenConstants;
import com.app.eaten.util.GpsTracker;
import com.app.eaten.util.PicassoMarker;
import com.app.eaten.util.Util;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.techery.properratingbar.ProperRatingBar;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements EatenConstants, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private View searchView;
    private ListView searchList;
    private CommonAdapter<EateryData> commonAdapter;

    private GoogleMap mMap;
    private FrameLayout mapLayout;
    private MenuItem mapMenuItem;
    private MenuItem gridMenuItem;
    private Marker marker;

    private ArrayList<EateryData> eateryList = new ArrayList<>();

    private AutoCompleteTextView atvPlaces;
    protected GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationManager lm;
    public Location mLastNetworkLocation;
    private PlacesTask placesTask;
    private Location location;
    private String key;
    private String latStr, lngStr;

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        searchView = inflater.inflate(R.layout.fragment_search, container, false);
        lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        setupViews();
        return searchView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_customer_fragment, menu);
        mapMenuItem = menu.findItem(R.id.map_view);
        gridMenuItem = menu.findItem(R.id.grid_view);
        mapMenuItem.setVisible(false);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (!mapMenuItem.isVisible() && !gridMenuItem.isVisible()) {
            gridMenuItem.setVisible(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map_view:
                gridMenuItem.setVisible(true);
                mapMenuItem.setVisible(false);
                searchList.setVisibility(View.GONE);
                mapLayout.setVisibility(View.VISIBLE);
                return true;
            case R.id.grid_view:
                gridMenuItem.setVisible(false);
                mapMenuItem.setVisible(true);
                searchList.setVisibility(View.VISIBLE);
                mapLayout.setVisibility(View.GONE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("latStr", latStr);
        outState.putString("lngStr", lngStr);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            // Restore last state for checked position.
            latStr = savedInstanceState.getString("latStr", null);
            lngStr = savedInstanceState.getString("lngStr", null);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void setupViews() {

        buildGoogleApiClient();

        atvPlaces = (AutoCompleteTextView) searchView.findViewById(R.id.txtPlace);
        atvPlaces.setThreshold(1);

        // Adding textchange listener
        atvPlaces.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (placesTask != null) {
                    placesTask.cancel(true);
                }
                placesTask = new PlacesTask();
                String loc = s.toString();
                loc = loc.trim();
                loc = loc.replaceAll(" ", "+");
                placesTask.execute(loc);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        searchList = (ListView) searchView.findViewById(R.id.listItems);

        commonAdapter = new CommonAdapter<>(getActivity(), eateryList);
        int[] viewIds = {R.id.eateryImage, R.id.eateryName, R.id.cusineType, R.id.costForTwo, R.id.rating_bar};
        commonAdapter.setLayoutTextViews(R.layout.search_eatery_list_item, viewIds);
        commonAdapter.setPopulationListener(new PopulationListener<EateryData>() {
            @Override
            public void populateFrom(View v, int position, EateryData row, View[] views) {
                if (row.getEateryLogo() != null) {
                    Picasso.with(getActivity()).load(SERVER_BASE_URL + row.getEateryLogo() + "?uniqueId=" + Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, "")).into((ImageView) views[0]);
                } else {
                    ((ImageView) views[0]).setImageResource(R.drawable.ic_eatery);
                }
                ((TextView) views[1]).setText(row.getEateryName());
                ((TextView) views[2]).setText(row.getEateryCusineType());
                ((TextView) views[3]).setText(row.getEateryCost());
                ((ProperRatingBar) views[4]).setRating(row.getEateryRating());
            }

            @Override
            public void onRowCreate(View[] views) {

            }
        });
        searchList.setAdapter(commonAdapter);
        searchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.getPrefs(getActivity()).edit().putString(EATERY_ID, eateryList.get(position).getEateryID()).apply();
                ((HomeActivity) getActivity()).addFragment(new EateryFragment(), false, false);
            }
        });

        mapLayout = (FrameLayout) searchView.findViewById(R.id.mapLayout);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
    }

    private void getEateryList(String areaORlati, String cityORlongi, boolean isGPSLoc) {

        Log.v("Test", "Fetching Eatery List");

        String url = SERVER_BASE_URL +
                GET_EATERY_LIST_URL + "?uniqueId=" + Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, "");
        if (isGPSLoc) {
            url += "&coordinate={\"latitude\":\"" + areaORlati + "\",\"longitude\":\"" + cityORlongi + "\",\"radius\":5}";
        } else {
            url += "&area=" + areaORlati + "&city=" + cityORlongi;
        }

        //url = "http://api.eatenapp.com/eatery?coordinate=%7B%22latitude%22:%2212.969880600004135%22,%22longitude%22:%2277.64078320002402%22,%22radius%22:5%7D&uniqueId=5628a5bae29e8";

        JsonObjectRequest getEateryRequest = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            eateryList.clear();
                            JSONArray jsonArray = response.getJSONArray("Eatery");
                            EateryData eateryData;
                            JSONObject jsonObject;
                            for (int i = 0; i < jsonArray.length(); i++) {
                                jsonObject = jsonArray.getJSONObject(i);
                                eateryData = new EateryData();
                                eateryData.setEateryID(jsonObject.optString("id"));
                                eateryData.setEateryName(jsonObject.optString("eateryName"));
                                eateryData.setEateryURL(jsonObject.optString("url"));
                                eateryData.setEateryCost(jsonObject.optString("costForTwo"));
                                JSONArray cuisineType = jsonObject.optJSONArray("cuisineType");
                                if (cuisineType != null) {
                                    int count = cuisineType.length();
                                    JSONObject cuisine;
                                    String data = "";
                                    for (int j = 0; j < count; j++) {
                                        cuisine = cuisineType.optJSONObject(j);
                                        if (cuisine != null) {
                                            data += (cuisine.getString("name") + ", ");
                                        }
                                    }
                                    eateryData.setEateryCusineType(data);
                                } else {
                                    eateryData.setEateryCusineType("NA");
                                }
                                eateryData.setEateryRating(jsonObject.optInt("rating"));
                                eateryData.setEateryLatitude(jsonObject.optDouble("latitude"));
                                eateryData.setEateryLongitude(jsonObject.optDouble("longitude"));
                                JSONArray logoArray = jsonObject.optJSONArray("logo");
                                if (logoArray != null) {
                                    if (logoArray.length() > 0) {
                                        eateryData.setEateryLogo(logoArray.getJSONObject(logoArray.length() - 1).optString("path"));
                                    }
                                }
                                eateryList.add(eateryData);
                            }
                            plotMarkers(eateryList);
                            commonAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "No Eateries in this Area!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        ((CommonActivity) getActivity()).addRequestToQueue(getEateryRequest);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if ( Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return  ;
        }
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Util.getPrefs(getActivity()).edit().putString(EATERY_ID, eateryList.get(Integer.parseInt(marker.getTitle())).getEateryID()).apply();
                ((HomeActivity) getActivity()).addFragment(new EateryFragment(), true, false);
                /*Fragment videoFragment = new EateryFragment();
                ((HomeActivity) getActivity()).addFragment(videoFragment, false, false);*/
                //FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                //transaction.replace(R.id.frame_layout, videoFragment).commit();
            }
        });
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                processNewLocations("" + latLng.latitude, "" + latLng.longitude);
            }
        });

    }

    private void plotMarkers(ArrayList<EateryData> eateryList) {
        try {
            Log.v("Eaten", "Plot");
            if (mMap != null) {
                if (eateryList.size() > 0) {
                    mMap.clear();
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    int length = eateryList.size();
                    int i = 0;
                    EateryData eatery;
                    LatLng markerLatLng;
                    PicassoMarker myMarker;
                    MarkerOptions markerOption;
                    for (; i < length; i++) {
                        eatery = eateryList.get(i);
                        // Create user marker with custom icon and other options
                        markerLatLng = new LatLng(eatery.getEateryLatitude(), eatery.getEateryLongitude());
                        markerOption = new MarkerOptions().position(markerLatLng);
                        /*if (eatery.getEateryLogo() != null) {
                            myMarker = new PicassoMarker(markerOption);
                            Picasso.with(getActivity()).load(SERVER_BASE_URL + eatery.getEateryLogo() + "?uniqueId=" + Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, "")).placeholder(R.drawable.ic_eatery).into(myMarker);
                        } else {
                            markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_eatery));
                        }*/
                        markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_eatery));

                        markerOption.title("" + i);

                        Log.v("Test", "Plotting marker -- " + "" + eatery.getEateryLatitude() + " ," + eatery.getEateryLongitude());

                        mMap.addMarker(markerOption);
                        builder.include(markerLatLng);
                        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker, 10.0f));
                        //mMarkersHashMap.put(currentMarker, myMarker);

                /*mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());*/
                    }
                    LatLngBounds bounds = builder.build();
                    int padding = 50; // offset from edges of the map in pixels
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private View view;

        public CustomInfoWindowAdapter() {
            view = LayoutInflater.from(getActivity()).inflate(R.layout.custom_eatery_window,
                    null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            if (SearchFragment.this.marker != null
                    && SearchFragment.this.marker.isInfoWindowShown()) {
                SearchFragment.this.marker.hideInfoWindow();
                SearchFragment.this.marker.showInfoWindow();
            }
            return null;
        }

        @Override
        public View getInfoWindow(final Marker marker) {
            SearchFragment.this.marker = marker;

            final String position = marker.getTitle();
            EateryData eatery = eateryList.get(Integer.parseInt(position));

            final ImageView image = ((ImageView) view.findViewById(R.id.eateryLogo));

            if (eatery.getEateryURL() != null) {
                Picasso.with(getActivity()).load(SERVER_BASE_URL + eatery.getEateryLogo() + "?uniqueId=" + Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, ""))
                        .placeholder(R.drawable.app_icon_small).fit().into(image);
            } else {
                image.setImageResource(R.drawable.app_icon_small);
            }

            TextView titleUi = ((TextView) view.findViewById(R.id.eateryName));
            titleUi.setText(eatery.getEateryName());
            TextView eateryCost = ((TextView) view.findViewById(R.id.eateryCost));
            eateryCost.setText(eatery.getEateryCost());
            TextView eateryType = ((TextView) view.findViewById(R.id.eateryType));
            eateryType.setText(eatery.getEateryCusineType());

            final ProperRatingBar ratingBar = ((ProperRatingBar) view
                    .findViewById(R.id.eateryRating));

            ratingBar.setEnabled(false);
            if (eatery.getEateryRating() != 0) {
                ratingBar.setRating(eatery.getEateryRating());
            } else {
                ratingBar.setRating(0);
            }
            ratingBar.invalidate();

            return view;
        }
    }

    private void processNewLocations(String lat, String lng) {
        Log.v("Test", "Inside Function processNewLocations");
        latStr = lat;
        lngStr = lng;
        getEateryList(lat, lng, true);
    }

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.v("Test", "Inside Function onConnected");
        // Provides a simple way of getting a device's location and is well
        // suited for
        // applications that do not require a fine-grained location and that do
        // not need location
        // updates. Gets the best and most recent location currently available,
        // which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        String atvPlacesContent = atvPlaces.getText().toString();
        if (atvPlacesContent.equalsIgnoreCase("")) {
            if (mLastLocation != null) {
                processNewLocations("" + mLastLocation.getLatitude(), "" + mLastLocation.getLongitude());
                GetLocationBackgroundTask obj = new GetLocationBackgroundTask(getActivity());
                obj.execute();
            } else {
                GpsTracker gps = new GpsTracker(getActivity());
                mLastLocation = gps.getLocation();
                if (mLastLocation != null) {
                    processNewLocations("" + mLastLocation.getLatitude(), "" + mLastLocation.getLongitude());
                    GetLocationBackgroundTask obj = new GetLocationBackgroundTask(getActivity());
                    obj.execute();
                } else {
                    atvPlaces.setHint("Enter Current Location");
                }
            }
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            if (latStr != null && latStr.length() > 0 && lngStr != null && lngStr.length() > 0) {
                processNewLocations(latStr, lngStr);
            } else {
                processNewLocations("" + mLastLocation.getLatitude(), "" + mLastLocation.getLongitude());
            }
            setLocation(mLastLocation);
        } else {
            GpsTracker gps = new GpsTracker(getActivity());
            Location mLastKnwonLocation = gps.getLocation();
            if (latStr != null && latStr.length() > 0 && lngStr != null && lngStr.length() > 0) {
                processNewLocations(latStr, lngStr);
                setLocation(mLastLocation);
            } else if (mLastKnwonLocation != null) {
                processNewLocations("" + mLastLocation.getLatitude(), "" + mLastLocation.getLongitude());
                setLocation(mLastLocation);
            } else {
                if (Build.VERSION.SDK_INT >= 23 &&
                        ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mLastNetworkLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (mLastNetworkLocation != null) {
                    processNewLocations("" + mLastLocation.getLatitude(), "" + mLastLocation.getLongitude());
                    setLocation(mLastLocation);
                }
            }
            /*if (mLastKnwonLocation == null && mLastNetworkLocation == null) {
                alert("This app needs Location Services. You can enable Location Services in your phone settings.");
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }*/
            // finish();
        }
        Log.v("Test", "Outside Function onConnected");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes
        // might be returned in
        // onConnectionFailed.
        // Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
        // + result.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We
        // call connect() to
        // attempt to re-establish the connection.
        // Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    private void setLocation(Location loc) {
        location = loc;
    }

    private class GetLocationBackgroundTask extends AsyncTask<String, String, String> {
        private Context con;
        private String currentLocation;
        private Geocoder geocoder;
        private List<Address> addresses;

        public GetLocationBackgroundTask(Context con) {
            this.con = con;
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
                if (addresses !=null && addresses.size() > 0) {
                    String[] addressPart = new String[4];
                    addressPart[2] = addresses.get(0).getAdminArea() + ", ";
                    addressPart[3] = addresses.get(0).getCountryName();
                    addressPart[0] = addresses.get(0).getSubLocality() + ", ";
                    addressPart[1] = addresses.get(0).getLocality() + ", ";
                    for (String anAddressPart : addressPart) {
                        if (!(anAddressPart.equalsIgnoreCase("null, ")))
                            currentLocation += anAddressPart;
                    }
                    if (currentLocation.contains("null")) {
                        currentLocation = currentLocation.replace("null", "");
                    }
                }

                // currentLocation = knownName + ", " + subLocality + ", " +
                // city + ", " + state + ", " + country;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
                currentLocation = "Enter Current Location";
            }
            return currentLocation;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            currentLocation = result;
            if (currentLocation == null || currentLocation.equalsIgnoreCase("Enter Current Location")) {
                Toast.makeText(con, "Unable to obtain location. Please enter your location manually.",
                        Toast.LENGTH_LONG).show();
                atvPlaces.setHint("Enter Your Location");
            } else {
                atvPlaces.setText(currentLocation);
                atvPlaces.setSelection(0);
                atvPlaces.clearFocus();
            }
        }
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            // key = "key=AIzaSyBxB6KbGdfhQpOnFFVpq8AgWBro2Gdy4e4";
            key = "key=" + SERVER_KEY;

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters;
            if (location == null) {
                parameters = input + "&" + sensor + "&" + key + "&radius=752360";
            } else {
                parameters = input + "&" + sensor + "&" + key + "&location=" + location.getLatitude() + ","
                        + location.getLongitude() + "&radius=752360";
            }
            // String parameters =
            // input+"&"+types+"&"+sensor+"&"+key+"&components=country:in";

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;
            Log.e("URL", url);
            try {
                // Fetching the data from web service in background
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Starting Parsing the JSON string returned by Web Service
            new ParserTask().execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(final List<HashMap<String, String>> result) {

            String[] from = new String[]{"description"};
            int[] to = new int[]{android.R.id.text1};

            // Creating a SimpleAdapter for the AutoCompleteTextView
            // SimpleAdapter adapter = new SimpleAdapter(getBaseContext(),
            // result, android.R.layout.simple_list_item_1, from, to);
            ArrayList<String> lst = new ArrayList<String>();

            if (result == null)
                return;

            for (HashMap<String, String> h : result) {
                lst.add(h.get("description"));

            }
            // String str=lst.toString();
            // atvPlaces.setText(str);
            FilterWithSpaceAdapter<String> adapter = new FilterWithSpaceAdapter<String>(getContext(),
                    R.layout.item_autocomplete, android.R.id.text1, lst);
            // Setting the adapter
            atvPlaces.setAdapter(adapter);
            atvPlaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                    // String selection =
                    // (String)parent.getItemAtPosition(position);
                    Log.v("Test", "Inside Function onItemClick");
                    String placeid = result.get(position).get("place_id");
                    Log.e("placeid", placeid);
                    new GetLatLong(placeid).execute();
                }
            });
        }
    }

    public class GetLatLong extends AsyncTask<Void, Void, String> {
        String placeid;

        public GetLatLong(String place) {
            placeid = place;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.v("Test", "Inside Function onPostExecute of GetLatLong Result " + result);
            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject jobj1 = jobj.getJSONObject("result");
                JSONObject jobj2 = jobj1.getJSONObject("geometry").getJSONObject("location");
                String lat = jobj2.getString("lat");
                String lng = jobj2.getString("lng");
                // latStr = lat;
                // lngStr = lng;

                processNewLocations(lat, lng);

                // To update offers Dynamically
                // if(offersFrag!=null)
                // {
                // try{
                // Location l = new Location(LocationManager.NETWORK_PROVIDER);
                // l.setLatitude(Double.parseDouble(lat));
                // l.setLongitude(Double.parseDouble(lng));
                // offersFrag.setLocations(l);
                // }
                // catch(Exception e){}
                // }
                Log.v("Test", "Outside Function onPostExecute of GetLatLong");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            String data = null;
            String url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeid + "&" + key;
            Log.e("URL", url);
            try {
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Eaten", "Exception while downloading url " + e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
}
