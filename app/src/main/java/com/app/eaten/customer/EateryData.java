package com.app.eaten.customer;

/**
 * Created by hl0395 on 15/8/15.
 */
public class EateryData {

    private String eateryID;
    private String eateryURL;
    private String eateryName;
    private String eateryCost;
    private String eateryCusineType;
    private int eateryRating;
    private double eateryLatitude;
    private double eateryLongitude;
    private String eateryLogo;


    public String getEateryID() {
        return eateryID;
    }

    public void setEateryID(String eateryID) {
        this.eateryID = eateryID;
    }

    public String getEateryURL() {
        return eateryURL;
    }

    public void setEateryURL(String eateryURL) {
        this.eateryURL = eateryURL;
    }

    public String getEateryName() {
        return eateryName;
    }

    public void setEateryName(String eateryName) {
        this.eateryName = eateryName;
    }

    public String getEateryCost() {
        return eateryCost;
    }

    public void setEateryCost(String eateryCost) {
        this.eateryCost = eateryCost;
    }

    public String getEateryCusineType() {
        return eateryCusineType;
    }

    public void setEateryCusineType(String eateryCusineType) {
        this.eateryCusineType = eateryCusineType;
    }

    public int getEateryRating() {
        return eateryRating;
    }

    public void setEateryRating(int eateryRating) {
        this.eateryRating = eateryRating;
    }

    public double getEateryLatitude() {
        return eateryLatitude;
    }

    public void setEateryLatitude(double eateryLatitude) {
        this.eateryLatitude = eateryLatitude;
    }

    public double getEateryLongitude() {
        return eateryLongitude;
    }

    public void setEateryLongitude(double eateryLongitude) {
        this.eateryLongitude = eateryLongitude;
    }

    public String getEateryLogo() {
        return eateryLogo;
    }

    public void setEateryLogo(String eateryLogo) {
        this.eateryLogo = eateryLogo;
    }
}
