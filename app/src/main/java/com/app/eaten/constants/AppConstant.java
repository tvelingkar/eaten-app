package com.app.eaten.constants;

import android.content.Context;
import android.database.Cursor;
import android.location.Address;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;

/**
 * Created by Tushar Velingkar on 2/12/15.
 */
public class AppConstant {

    public static Address EATERY_ADDRESS;

    public static final String EVENT_EATERY_SELECTED = "EATERY_SELECTED";

    public static final String EVENT_EATERY_LOC_SELECTED = "EATERY_LOC_SELECTED";

    public static final String EVENT_EATERY_DATA_FETCHED = "EATERY_DATA_FETCHED";

    private static final ArrayList<String> PAYMENT_OPTIONS = new ArrayList<>();

    public static final ArrayList<String> getPaymentOptions() {
        PAYMENT_OPTIONS.add("Cash");
        PAYMENT_OPTIONS.add("Card");
        PAYMENT_OPTIONS.add("Cheque");

        return PAYMENT_OPTIONS;
    }

    public static final String getPath(Uri contentUri, Context context) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }
}
