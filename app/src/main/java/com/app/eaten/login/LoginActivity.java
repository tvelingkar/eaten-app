package com.app.eaten.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.eaten.CommonActivity;
import com.app.eaten.R;
import com.app.eaten.home.HomeActivity;
import com.app.eaten.util.Util;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends CommonActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private CallbackManager callbackManager;
    private EditText userName, password;
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog progressDialog;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();

        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }

        setContentView(R.layout.activity_login);

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        callbackManager = CallbackManager.Factory.create();

        LoginButton facebookLogin = (LoginButton) findViewById(R.id.facebook_login);
        Button registrationBtn = (Button) findViewById(R.id.registrationBtn);
        registrationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("Sign Up");
                final FrameLayout frameView = new FrameLayout(LoginActivity.this);
                builder.setView(frameView);

                final AlertDialog alertDialog = builder.create();
                LayoutInflater inflater = alertDialog.getLayoutInflater();
                View dialoglayout = inflater.inflate(R.layout.sign_up, frameView);

                final EditText email = (EditText) dialoglayout.findViewById(R.id.emailEditText);
                final EditText fName = (EditText) dialoglayout.findViewById(R.id.firstNameEditText);
                final EditText lName = (EditText) dialoglayout.findViewById(R.id.lastNameEditText);
                final EditText password = (EditText) dialoglayout.findViewById(R.id.passwordEditText);
                final EditText mobileNo = (EditText) dialoglayout.findViewById(R.id.mobileNoEditText);
                Button signupBtn = (Button) dialoglayout.findViewById(R.id.signUpButton);

                signupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (fName.getText().toString().isEmpty() || lName.getText().toString().isEmpty() || mobileNo.getText().toString().isEmpty()) {
                            Toast.makeText(LoginActivity.this, "First Name or Last Name or Mobile No cannot be empty", Toast.LENGTH_LONG).show();
                        } else {
                            sendSignupRequest(email.getText().toString(), fName.getText().toString(), lName.getText().toString(), password.getText().toString(), mobileNo.getText().toString());

                            alertDialog.dismiss();
                        }
                    }
                });

                alertDialog.show();

            }
        });


//        facebookLogin.setReadPermissions("public_profile");
        facebookLogin.setReadPermissions(Collections.singletonList("public_profile, email, user_birthday"));
        facebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Util.getPrefs(LoginActivity.this).edit().putBoolean(LOGIN_STATUS, true).commit();
                Util.getPrefs(LoginActivity.this).edit().putBoolean(IS_FACEBOOK_LOGIN, true).commit();
                Log.v("Eaten", "First Time " + Util.getPrefs(LoginActivity.this).getBoolean(IS_FIRST_TIME, false));
                if (Util.getPrefs(LoginActivity.this).getBoolean(IS_FIRST_TIME, true)) {
                    progressDialog = new ProgressDialog(LoginActivity.this);
                    progressDialog.setTitle("Please wait . . .");
                    progressDialog.setIndeterminate(true);
                    progressDialog.show();
                    GraphRequest request = GraphRequest.newMeRequest(
                            loginResult.getAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(
                                        JSONObject object,
                                        GraphResponse response) {
                                    if (object != null) {
                                        try {
                                            Util.getPrefs(LoginActivity.this).edit().putString(USER_FNAME, object.optString("first_name")).commit();
                                            Util.getPrefs(LoginActivity.this).edit().putString(USER_LNAME, object.optString("last_name")).commit();
                                            Util.getPrefs(LoginActivity.this).edit().putString(USER_GENDER, object.optString("gender")).commit();
                                            Util.getPrefs(LoginActivity.this).edit().putString(USER_DOB, object.optString("birthday")).commit();
                                            Util.getPrefs(LoginActivity.this).edit().putString(USER_EMAIL, object.optString("email")).commit();
                                            if (!object.optString("picture").isEmpty()) {
                                                JSONObject data = object.optJSONObject("picture").getJSONObject("data");
                                                object.put("picture", data.optString("url"));
                                                Util.getPrefs(LoginActivity.this).edit().putString(USER_PROFILE_URI, data.optString("url")).commit();
                                                saveImageToLocal(data.optString("url"), object.optString("first_name"));
                                            }
                                            object.put("facebook_id", object.optString("id"));
                                            object.put("user_type", Util.getPrefs(LoginActivity.this).getInt(SELECTION_VALUE, CUSTOMER_SELECTION));

                                            sendLoginRequest(object, SIGNIN_FACEBOOK_URL);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,birthday,first_name,last_name,gender,picture.type(large),email");
                    request.setParameters(parameters);
                    request.executeAsync();
                } else {
                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }

            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e("Error", "Error --- " + exception.getCause());
            }
        });

        userName = (EditText) findViewById(R.id.userNameEditTxt);
        password = (EditText) findViewById(R.id.passwordEditTxt);
        Button loginBtn = (Button) findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject request = new JSONObject();
                try {
                    request.put("username", userName.getText().toString());
                    request.put("password", password.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                sendLoginRequest(request, SIGNIN_URL);
            }
        });

        Button googleLogin = (Button) findViewById(R.id.google_login);
        googleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("Eaten", "Google Login Clicked");
                signInWithGplus();
            }
        });

        setCountryCode();
    }

    private void sendLoginRequest(JSONObject data, String url) {

        JSONObject loginRequestObj = new JSONObject();

        try {
            loginRequestObj.putOpt("user", data);
            Log.v("Eaten", "Request -- " + loginRequestObj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest loginRequest = new JsonObjectRequest(Request.Method.POST, SERVER_BASE_URL + url, loginRequestObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(LoginActivity.this, "Successfully logged in!!!", Toast.LENGTH_SHORT).show();
                Log.d("TAG", "response --- " + response.toString());

                try {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    Util.getPrefs(LoginActivity.this).edit().putInt(SELECTION_VALUE, Integer.parseInt(response.getString("userType"))).apply();
                    Util.getPrefs(LoginActivity.this).edit().putString(LOGIN_UNIQUE_ID, response.getString("uniqueId")).apply();
                    Util.getPrefs(LoginActivity.this).edit().putBoolean(LOGIN_STATUS, true).apply();

                    LoginActivity.this.setResult(Activity.RESULT_OK);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this, "Incorrect Username or Password", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        addRequestToQueue(loginRequest);

    }

    private void sendSignupRequest(final String email, final String fName, final String lName, String password, String mobileNo) {
        JSONObject signUpRequestObj = new JSONObject();

        JSONObject request = new JSONObject();
        try {
            Util.getPrefs(this).edit().putString(USER_MOBILE, mobileNo).apply();
            request.put("mobile", Util.getPrefs(this).getString(COUNTRY_CODE, "+91") + mobileNo);
            request.put("email", email);
            request.put("firstName", fName);
            request.put("lastName", lName);
            request.put("password", password);
            request.put("user_type", Util.getPrefs(this).getInt(SELECTION_VALUE, CUSTOMER_SELECTION));

            signUpRequestObj.putOpt("user", request);
            Log.v("Eaten", "Request -- " + signUpRequestObj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest signUpRequest = new JsonObjectRequest(Request.Method.POST, SERVER_BASE_URL + SIGNUP_URL, signUpRequestObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if ("User Allready registered".equals(response.getString("status"))) {
                        Toast.makeText(LoginActivity.this, response.getString("status") + " Please login with your credentials", Toast.LENGTH_SHORT).show();

                    } else if ("success".equals(response.getString("status"))) {
                        Toast.makeText(LoginActivity.this, "Successfully signed up!!!", Toast.LENGTH_SHORT).show();

                        Util.getPrefs(LoginActivity.this).edit().putInt(SELECTION_VALUE, Integer.parseInt(response.getString("userType"))).commit();
                        Util.getPrefs(LoginActivity.this).edit().putString(LOGIN_UNIQUE_ID, response.getString("uniqueId")).commit();
                        Util.getPrefs(LoginActivity.this).edit().putBoolean(LOGIN_STATUS, true).commit();
                        Util.getPrefs(LoginActivity.this).edit().putString(USER_EMAIL, email).commit();
                        Util.getPrefs(LoginActivity.this).edit().putString(USER_FNAME, fName).commit();
                        Util.getPrefs(LoginActivity.this).edit().putString(USER_LNAME, lName).commit();

                        LoginActivity.this.setResult(Activity.RESULT_OK);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this, "Error while signing up!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        addRequestToQueue(signUpRequest);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v("Eaten", "Connect started");
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            Log.v("Eaten", "Disconnect started");
            mGoogleApiClient.disconnect();
        }
    }

    private void saveImageToLocal(String imageUrl, final String fileName) {
        Target target = new Target() {

            @Override
            public void onPrepareLoad(Drawable arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom arg1) {
                String path = fileName + ".jpg";
                File file = new File(EATEN_IMAGES_PRIVATE, fileName + ".jpg");
                try {
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    FileOutputStream ostream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 75, ostream);
                    ostream.close();
                    Util.getPrefs(LoginActivity.this).edit().putString(USER_PROFILE_URI, path).apply();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onBitmapFailed(Drawable arg0) {
                // TODO Auto-generated method stub

            }
        };
        if ("".equals(imageUrl)){
            Picasso.with(this).load(R.drawable.add_photo).into(target);
        } else {
            Picasso.with(this).load(imageUrl).into(target);
        }
    }

    private void setCountryCode() {
        JsonObjectRequest getCountryName = new JsonObjectRequest(Request.Method.GET, GET_COUNTRY_NAME_URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    String countryName = response.optString("country");
                    Util.getPrefs(LoginActivity.this).edit().putString(COUNTRY_NAME, countryName).apply();
                    String url = String.format(GET_COUNTRY_CODE_URL + "%s", countryName);
                    JsonArrayRequest getCountryCode = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {

                            if (response != null) {
                                Util.getPrefs(LoginActivity.this).edit().putString(COUNTRY_CODE, String.format("+%s", response.optJSONObject(0).optJSONArray("callingCodes").optString(0))).apply();
                                Util.getPrefs(LoginActivity.this).edit().putString(COUNTRY_CURRENCY, response.optJSONObject(0).optJSONArray("currencies").optString(0)).apply();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.v("Eaten", "Error  while fetching country code");
                        }
                    }) {
                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8";
                        }
                    };
                    addRequestToQueue(getCountryCode);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        addRequestToQueue(getCountryName);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.v("Eaten", "Google Connected");
        mSignInClicked = false;
        // Get user's information
        getProfileInformation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v("Eaten", "Google Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v("Eaten", "Google Connection failed");
        if (!connectionResult.hasResolution()) {
            Log.v("Eaten", "Has No Resolution");
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this,
                    0).show();
            return;
        }
        Log.v("Eaten", "Has Resolution");
        if (!mIntentInProgress) {
            Log.v("Eaten", "mIntentInProgress False");
            // Store the ConnectionResult for later usage
            mConnectionResult = connectionResult;
            Log.v("Eaten", "Sign in Clicked -- " + mSignInClicked);
            if (mSignInClicked) {
                Log.v("Eaten", "SignInClicked True");
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
    }

    private void resolveSignInError() {
        Log.v("Eaten", "In resolveSignInError");
        if (mConnectionResult.hasResolution()) {
            Log.v("Eaten", "If Has Resolution");
            try {
                mIntentInProgress = true;
                Log.v("Eaten", "Start ResolutionforResult started");
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                Log.v("Eaten", "Exception occured while starting");
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    private void getProfileInformation() {
        try {
            Log.v("Eaten", "In getProfileInformation");
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String personGooglePlusProfile = currentPerson.getUrl();
                // by default the profile url gives 50x50 px image only
                // we can replace the value with whatever dimension we want by
                // replacing sz=X
                personPhotoUrl = personPhotoUrl.substring(0,
                        personPhotoUrl.length() - 2)
                        + 400;

                String gender = "";
                if (currentPerson.getGender() == Person.Gender.MALE) {
                    gender = "Male";
                } else if (currentPerson.getGender() == Person.Gender.FEMALE) {
                    gender = "FEMALE";
                } else if (currentPerson.getGender() == Person.Gender.OTHER) {
                    gender = "OTHER";
                }
                Util.getPrefs(LoginActivity.this).edit().putBoolean(LOGIN_STATUS, true).commit();
                Util.getPrefs(LoginActivity.this).edit().putBoolean(IS_GOOGLE_LOGIN, true).commit();
                if (Util.getPrefs(LoginActivity.this).getBoolean(IS_FIRST_TIME, true)) {
                    try {
                        Util.getPrefs(LoginActivity.this).edit().putString(USER_FNAME, personName.split(" ")[0]).commit();
                        Util.getPrefs(LoginActivity.this).edit().putString(USER_LNAME, personName.split(" ")[1]).commit();
                        Util.getPrefs(LoginActivity.this).edit().putString(USER_GENDER, gender).commit();
                        Util.getPrefs(LoginActivity.this).edit().putString(USER_DOB, currentPerson.getBirthday()).commit();
                        Util.getPrefs(LoginActivity.this).edit().putString(USER_EMAIL, Plus.AccountApi.getAccountName(mGoogleApiClient)).commit();
                        Util.getPrefs(LoginActivity.this).edit().putString(USER_PROFILE_URI, personPhotoUrl).commit();
                        saveImageToLocal(personPhotoUrl, personName.split(" ")[0]);

                        JSONObject object = new JSONObject();
                        object.put("email", Plus.AccountApi.getAccountName(mGoogleApiClient));
                        object.put("given_name", personName.split(" ")[0]);
                        object.put("family_name", personName.split(" ")[1]);
                        object.put("gender", gender);
                        object.put("google_id", currentPerson.getId());
                        object.put("user_type", Util.getPrefs(LoginActivity.this).getInt(SELECTION_VALUE, CUSTOMER_SELECTION));
                        object.put("picture", personPhotoUrl);
                        sendLoginRequest(object, SIGNIN_GOOGLE_URL);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void signInWithGplus() {
        Log.v("Eaten", "In signInWithGplus");
        if (!mGoogleApiClient.isConnecting()) {
            Log.v("Eaten", "If is Not Connecting");
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    @Override
    public void onBackPressed() {
        LoginActivity.this.setResult(ON_BACK_PRESSED);
        finish();
    }
}
