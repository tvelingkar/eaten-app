package com.app.eaten.login;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.eaten.CommonActivity;
import com.app.eaten.R;
import com.app.eaten.beans.CommonTypeObject;
import com.app.eaten.fragments.DatePickerFragment;
import com.app.eaten.util.CuisineTypeOptions;
import com.app.eaten.util.EatenConstants;
import com.app.eaten.util.EateryTypeOptions;
import com.app.eaten.util.UploadFileToServer;
import com.app.eaten.util.Util;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationActivity extends CommonActivity implements AdapterView.OnItemClickListener {

    private EditText dobEdit, emailEdit, firstNameEdit, lastNameEdit, mobileNoEdit, occupation, cookingExperience, description;
    private ToggleButton sexToggle;
    private SwitchCompat foodieToggle;
    private CircleImageView userProfilePicImg;
    private AutoCompleteTextView cityEdit;
    private String path;
    private String fileName;
    private static String countryName;


    public RegistrationActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registration);
        setupViews();
        logUser();
    }

    private void setupViews() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        emailEdit = (EditText) findViewById(R.id.emailEdit);
        firstNameEdit = (EditText) findViewById(R.id.firstNameEdit);
        lastNameEdit = (EditText) findViewById(R.id.lastNameEdit);

        dobEdit = (EditText) findViewById(R.id.dobValue);
        TextView countryCode = (TextView) findViewById(R.id.countryCode);
        sexToggle = (ToggleButton) findViewById(R.id.sexToggle);
        cityEdit = (AutoCompleteTextView) findViewById(R.id.cityEdit);
        cityEdit.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));
        cityEdit.setOnItemClickListener(this);

        mobileNoEdit = (EditText) findViewById(R.id.mobileNoEdit);
        foodieToggle = (SwitchCompat) findViewById(R.id.foodieToggle);
        occupation = (EditText) findViewById(R.id.occupationEdit);
        cookingExperience = (EditText) findViewById(R.id.cookingExpEdit);
        description = (EditText) findViewById(R.id.descriptionEdit);
        userProfilePicImg = (CircleImageView) findViewById(R.id.userProfilePicImg);

        if (Util.getPrefs(RegistrationActivity.this).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == CUSTOMER_SELECTION) {
            occupation.setVisibility(View.GONE);
            cookingExperience.setVisibility(View.GONE);
            description.setVisibility(View.GONE);
        } else {
            occupation.setVisibility(View.VISIBLE);
            cookingExperience.setVisibility(View.VISIBLE);
            description.setVisibility(View.VISIBLE);

        }

        emailEdit.setText(Util.getPrefs(this).getString(USER_EMAIL, ""));
        firstNameEdit.setText(Util.getPrefs(this).getString(USER_FNAME, ""));
        lastNameEdit.setText(Util.getPrefs(this).getString(USER_LNAME, ""));
        dobEdit.setText(Util.getPrefs(this).getString(USER_DOB, ""));
        mobileNoEdit.setText(Util.getPrefs(this).getString(USER_MOBILE, ""));
        countryCode.setText(Util.getPrefs(this).getString(COUNTRY_CODE, ""));
        if ("male".equalsIgnoreCase(Util.getPrefs(this).getString(USER_GENDER, ""))) {
            sexToggle.setChecked(false);
        } else if ("female".equalsIgnoreCase(Util.getPrefs(this).getString(USER_GENDER, ""))) {
            sexToggle.setChecked(true);
        }
        if ("Y".equalsIgnoreCase(Util.getPrefs(this).getString(USER_FOODIE, ""))) {
            foodieToggle.setChecked(true);
        } else if ("N".equalsIgnoreCase(Util.getPrefs(this).getString(USER_FOODIE, ""))) {
            foodieToggle.setChecked(false);
        }
        cityEdit.setText(Util.getPrefs(this).getString(USER_CITY, ""));
        occupation.setText(Util.getPrefs(this).getString(USER_OCCUPATION, ""));
        cookingExperience.setText(Util.getPrefs(this).getString(USER_COOKING_EXP, ""));
        description.setText(Util.getPrefs(this).getString(USER_DESCRIPTION, ""));

        Button registerBtn = (Button) findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateData()) {
                    sendRegistrationRequest();
                } else {
                    final AlertDialog builder = new AlertDialog.Builder(RegistrationActivity.this).create();
                    builder.setTitle("Some Fields are Empty");
                    builder.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            builder.dismiss();
                        }
                    });
                    builder.show();
                }
            }
        });
        if (Util.getPrefs(this).getBoolean(IS_FIRST_TIME, true)) {
            registerBtn.setText(getResources().getString(R.string.register_txt));
        }

        userProfilePicImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

                //path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/eatery_"+timeStamp+".jpg";

                /*Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(new File(path)));
                startActivityForResult(intent, 1);*/

                selectImage();
            }
        });

        final DatePickerFragment datePickerFragment = new DatePickerFragment();
        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dobEdit.setText(String.format("%s/%s/%s", dayOfMonth, monthOfYear + 1, year));
            }
        };
        datePickerFragment.setDateListener(onDateSetListener);
        dobEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerFragment.show(getFragmentManager().beginTransaction(), "datePicker");
            }
        });

        //if (Util.getPrefs(this).getBoolean(IS_REG_FAILED, false) || Util.getPrefs(this).getBoolean(IS_FACEBOOK_LOGIN, false) || Util.getPrefs(this).getBoolean(IS_GOOGLE_LOGIN, false)) {
            /*if (Util.getPrefs(this).getBoolean(IS_FACEBOOK_LOGIN, false) || Util.getPrefs(this).getBoolean(IS_GOOGLE_LOGIN, false)) {
                if (!"".equals(Util.getPrefs(this).getString(USER_PROFILE_URI, ""))) {
                    Log.v("Eaten", "profile URL " + Util.getPrefs(this).getString(USER_PROFILE_URI, ""));
                    Picasso.with(this).load(Util.getPrefs(this).getString(USER_PROFILE_URI, ""))
                            .placeholder(ContextCompat.getDrawable(this, R.drawable.add_photo))
                            .fit()
                            .into(userProfilePicImg);
                }
            } else {*/
        if (!"".equals(Util.getPrefs(this).getString(USER_PROFILE_URI, ""))) {
            Picasso.with(this).load(Util.getPrefs(this).getString(USER_PROFILE_URI, ""))
                    .placeholder(ContextCompat.getDrawable(this, R.drawable.add_photo))
                    .fit()
                    .into(userProfilePicImg);
        }
        //}

        //}

        if (Util.getPrefs(this).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
            if (OPTION_NO.equals(Util.getPrefs(this).getString(IS_EATERY_CREATED, OPTION_NO))) {
                if (EateryTypeOptions.getCount() == 0 || CuisineTypeOptions.getCount() == 0) {
                    fetchEateryOptions();
                }
            }
        }

        countryName = Util.getPrefs(RegistrationActivity.this).getString(COUNTRY_NAME, "");

    }

    private boolean validateData() {
        if (firstNameEdit.getText().toString().isEmpty()) {
            return false;
        } else if (lastNameEdit.getText().toString().isEmpty()) {
            return false;
        } else if (dobEdit.getText().toString().isEmpty()) {
            return false;
        } else if (cityEdit.getText().toString().isEmpty()) {
            return false;
        } else if (mobileNoEdit.getText().toString().isEmpty()) {
            return false;
        }
        if (Util.getPrefs(RegistrationActivity.this).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
            if (occupation.getText().toString().isEmpty()) {
                return false;
            } else if (cookingExperience.getText().toString().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private void sendRegistrationRequest() {

        JSONObject updateProfileRequestObj = new JSONObject();

        JSONObject request = new JSONObject();
        try {

            request.put("email", emailEdit.getText().toString());
            request.put("fname", firstNameEdit.getText().toString());
            request.put("lname", lastNameEdit.getText().toString());
            request.put("dob", dobEdit.getText().toString());
            request.put("city", cityEdit.getText().toString());
            request.put("mobile", mobileNoEdit.getText().toString());
            if (foodieToggle.isChecked()) {
                request.put("foodie", "Y");
                Util.getPrefs(this).edit().putString(USER_FOODIE, "Y").apply();
            } else {
                request.put("foodie", "N");
                Util.getPrefs(this).edit().putString(USER_FOODIE, "N").apply();
            }
            request.put("gender", sexToggle.getText().toString());
            request.put("occupation", occupation.getText().toString());
            request.put("description", description.getText().toString());
            request.put("cookingExperience", cookingExperience.getText().toString());
            request.put("uniqueId", Util.getPrefs(this).getString(EatenConstants.LOGIN_UNIQUE_ID, ""));
            request.put("age", "10");

            Util.getPrefs(this).edit().putString(USER_EMAIL, emailEdit.getText().toString()).apply();
            Util.getPrefs(this).edit().putString(USER_FNAME, firstNameEdit.getText().toString()).apply();
            Util.getPrefs(this).edit().putString(USER_LNAME, lastNameEdit.getText().toString()).apply();
            Util.getPrefs(this).edit().putString(USER_DOB, dobEdit.getText().toString()).apply();
            Util.getPrefs(this).edit().putString(USER_CITY, cityEdit.getText().toString()).apply();
            Util.getPrefs(this).edit().putString(USER_MOBILE, mobileNoEdit.getText().toString()).apply();
            Util.getPrefs(this).edit().putString(USER_GENDER, sexToggle.getText().toString()).apply();
            Util.getPrefs(this).edit().putString(USER_OCCUPATION, occupation.getText().toString()).apply();
            Util.getPrefs(this).edit().putString(USER_COOKING_EXP, cookingExperience.getText().toString()).apply();
            Util.getPrefs(this).edit().putString(USER_DESCRIPTION, description.getText().toString()).apply();

            updateProfileRequestObj.putOpt("user", request);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("TAG", "Request : " + updateProfileRequestObj.toString());

        JsonObjectRequest registrationRequest = new JsonObjectRequest(Request.Method.POST, SERVER_BASE_URL + UPDATE_USER_INFO_URL, updateProfileRequestObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("TAG", "Response : " + response.toString());

                RegistrationActivity.this.setResult(RESULT_OK);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                RegistrationActivity.this.setResult(RESULT_CANCELED);
                finish();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        addRequestToQueue(registrationRequest);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            File imgFile = null;
            if (requestCode == TAKE_PIC_REQ_CODE) {
                File file = Environment.getExternalStorageDirectory();
                for (File temp : file.listFiles()) {
                    if (temp.getName().equals(fileName)) {
                        file = temp;
                        break;
                    }
                }
                try {
                    Bitmap bm;

                    String fileName = file.getAbsolutePath();
                    bm = Util.decodeSampledBitmapFromPath(fileName, 100, 100);

                    imgFile = getFileFromBitmap(bm);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CHOOSE_LIB_REQ_CODE) {
                Uri selectedImageUri = data.getData();

                path = getPath(selectedImageUri, this);

                imgFile = new File(path);

                fileName = imgFile.getName();

                //ContentResolver cR = this.getContentResolver();
                //Log.v("Eaten", "File MIME type -- " + cR.getType(selectedImageUri));

            }

            if (imgFile != null) {
                if (imgFile.exists()) {

                    Util.getPrefs(this).edit().putString(USER_PROFILE_URI, path).apply();
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                    userProfilePicImg.setImageBitmap(myBitmap);

                    UploadFileToServer uploadFileToServer = new UploadFileToServer(imgFile, this, UPLOAD_USER_PIC_URL, UPLOAD_USER_PIC);
                    uploadFileToServer.execute();
                }
            }
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        String str = (String) adapterView.getItemAtPosition(position);
    }

    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + SERVER_KEY);
            sb.append("&components=country:" + countryName);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("Eaten", "Error processing Places API URL", e);
            return null;
        } catch (IOException e) {
            Log.e("Eaten", "Error connecting to Places API", e);
            return null;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e("Eaten", "Cannot process JSON results", e);
        }

        return resultList;
    }

    private class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public String getPath(Uri contentUri, Context context) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

                    fileName = "user_profile_" + timeStamp + ".jpg";
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(EATEN_IMAGES_PRIVATE, fileName);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    dialog.dismiss();
                    startActivityForResult(intent, TAKE_PIC_REQ_CODE);
                    // dispatchTakePictureIntent();
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    dialog.dismiss();
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            CHOOSE_LIB_REQ_CODE);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private File getFileFromBitmap(Bitmap bitmap) {
        try {
            //create a file to write bitmap data
            File f = new File(Environment.getExternalStorageDirectory(), fileName);
            f.createNewFile();

//Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in fileUSER_PROFILE_URI
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

            return f;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void fetchEateryOptions() {

        JsonObjectRequest updateMenuRequest = new JsonObjectRequest(Request.Method.GET, SERVER_BASE_URL +
                FETCH_EATERY_CUISINE_TYPE + "?uniqueId=" + Util.getPrefs(this).getString(LOGIN_UNIQUE_ID, ""), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                JSONArray objectType = response.optJSONArray("EateryType");
                int count;
                int i;
                JSONObject object;
                if (objectType != null) {
                    count = objectType.length();
                    CommonTypeObject typeObject;
                    for (i = 0; i < count; i++) {
                        object = objectType.optJSONObject(i);
                        if (object != null) {
                            typeObject = new CommonTypeObject();
                            typeObject.id = object.optInt("id");
                            typeObject.name = object.optString("name");
                            EateryTypeOptions.addEntry(typeObject);
                        }
                    }
                }

                objectType = response.optJSONArray("CuisineType");
                if (objectType != null) {
                    count = objectType.length();
                    CommonTypeObject typeObject;
                    for (i = 0; i < count; i++) {
                        object = objectType.optJSONObject(i);
                        if (object != null) {
                            typeObject = new CommonTypeObject();
                            typeObject.id = object.optInt("id");
                            typeObject.name = object.optString("name");
                            CuisineTypeOptions.addEntry(typeObject);
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        addRequestToQueue(updateMenuRequest);
    }

    private void logUser() {
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(Util.getPrefs(this).getString(LOGIN_UNIQUE_ID, ""));
        Crashlytics.setUserEmail(Util.getPrefs(this).getString(USER_EMAIL, ""));
        Crashlytics.setUserName(Util.getPrefs(this).getString(USER_FNAME, ""));
    }

    @Override
    public void onBackPressed() {
        RegistrationActivity.this.setResult(ON_BACK_PRESSED);
        finish();
    }

}
