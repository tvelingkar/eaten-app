package com.app.eaten.activities;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.app.eaten.CommonActivity;
import com.app.eaten.R;
import com.app.eaten.util.Util;
import com.squareup.picasso.Picasso;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tushar Velingkar on 29/12/2015
 */
public class ActivityViewProfile extends CommonActivity {

    public ActivityViewProfile() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_profile);
        setupViews();
    }

    private void setupViews() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        TextView firstNameEdit = (TextView) findViewById(R.id.firstNameEdit);
        TextView lastNameEdit = (TextView) findViewById(R.id.lastNameEdit);

        TextView dobEdit = (TextView) findViewById(R.id.dobValue);
        TextView sexToggle = (TextView) findViewById(R.id.sexToggle);
        TextView cityEdit = (TextView) findViewById(R.id.cityEdit);

        TextView mobileNoEdit = (TextView) findViewById(R.id.mobileNoEdit);
        SwitchCompat foodieToggle = (SwitchCompat) findViewById(R.id.foodieToggle);
        TextView occupation = (TextView) findViewById(R.id.occupationEdit);
        TextView cookingExperience = (TextView) findViewById(R.id.cookingExpEdit);
        TextView description = (TextView) findViewById(R.id.descriptionEdit);
        CircleImageView userProfilePicImg = (CircleImageView) findViewById(R.id.userProfilePicImg);

        if (Util.getPrefs(ActivityViewProfile.this).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == CUSTOMER_SELECTION) {
            occupation.setVisibility(View.GONE);
            cookingExperience.setVisibility(View.GONE);
            description.setVisibility(View.GONE);
        } else {
            occupation.setVisibility(View.VISIBLE);
            cookingExperience.setVisibility(View.VISIBLE);
            description.setVisibility(View.VISIBLE);
            occupation.setText(Util.getPrefs(this).getString(USER_OCCUPATION, ""));
            cookingExperience.setText(Util.getPrefs(this).getString(USER_COOKING_EXP, ""));
            description.setText(Util.getPrefs(this).getString(USER_DESCRIPTION, ""));
        }

        firstNameEdit.setText(Util.getPrefs(this).getString(USER_FNAME, ""));
        lastNameEdit.setText(Util.getPrefs(this).getString(USER_LNAME, ""));
        dobEdit.setText(Util.getPrefs(this).getString(USER_DOB, ""));
        cityEdit.setText(Util.getPrefs(this).getString(USER_CITY, ""));
        mobileNoEdit.setText(Util.getPrefs(this).getString(COUNTRY_CODE, "") + Util.getPrefs(this).getString(USER_MOBILE, ""));
        sexToggle.setText(Util.getPrefs(this).getString(USER_GENDER, ""));
        if ("Y".equalsIgnoreCase(Util.getPrefs(this).getString(USER_FOODIE, ""))) {
            foodieToggle.setChecked(true);
        } else if ("N".equalsIgnoreCase(Util.getPrefs(this).getString(USER_FOODIE, ""))) {
            foodieToggle.setChecked(false);
        }

        Picasso.with(this).load(Util.getPrefs(this).getString(USER_PROFILE_URI, ""))
                .placeholder(ContextCompat.getDrawable(this, R.drawable.add_photo))
                .fit()
                .into(userProfilePicImg);

    }

}
