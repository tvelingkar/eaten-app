package com.app.eaten.activities;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Tushar Velingkar on 29/11/15.
 */
public class EateryApplication extends Application {

    private static Context context;

    public static Context getEateryAppContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        context = getApplicationContext();
    }
}
