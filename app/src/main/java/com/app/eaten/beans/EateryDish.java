package com.app.eaten.beans;

/**
 * Created by Tushar Velingkar on 4/12/15.
 */
public class EateryDish {

    public int imageId;

    public String imageUrl;

    public String dishName;

    public int dishPrice;

    public String imageLocalUri;
}
