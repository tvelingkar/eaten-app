package com.app.eaten.beans;

/**
 * Created by Tushar Velingkar on 22/12/15.
 */
public class ReviewItem {
    public int reviewId;
    public String userId;
    public int reviewRating;
    public String reviewContent;
    public String reviewDate;
}
