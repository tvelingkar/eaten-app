package com.app.eaten.beans;

import java.io.Serializable;

/**
 * Created by Tushar Velingkar on 15/12/15.
 */
public class EateryObject implements Serializable {

    private String eateryLogo;
    private String eateryName;
    private String yearOfEstablishment;
    private String address;
    private double latitude;
    private double longitude;
    private String mobileNo;
    private String cuisineType;
    private String eateryType;
    private String paymentType;
    private boolean isBreakfastAvailable;
    private boolean isLunchAvailable;
    private boolean isDinnerAvailable;
    private boolean isDeliveryAvailable;
    private boolean isCateringAvailable;
    private String chefName;
    private String aboutEatery;
    private int costForTwo;
    private String city;
    private String area;
    private String country;
    private String pincode;
    private String state;

    public String getEateryLogo() {
        return eateryLogo;
    }

    public void setEateryLogo(String eateryLogo) {
        this.eateryLogo = eateryLogo;
    }

    public String getEateryName() {
        return eateryName;
    }

    public void setEateryName(String eateryName) {
        this.eateryName = eateryName;
    }

    public String getYearOfEstablishment() {
        return yearOfEstablishment;
    }

    public void setYearOfEstablishment(String yearOfEstablishment) {
        this.yearOfEstablishment = yearOfEstablishment;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getCuisineType() {
        return cuisineType;
    }

    public void setCuisineType(String cuisineType) {
        this.cuisineType = cuisineType;
    }

    public String getEateryType() {
        return eateryType;
    }

    public void setEateryType(String eateryType) {
        this.eateryType = eateryType;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public boolean isBreakfastAvailable() {
        return isBreakfastAvailable;
    }

    public void setIsBreakfastAvailable(boolean isBreakfastAvailable) {
        this.isBreakfastAvailable = isBreakfastAvailable;
    }

    public boolean isLunchAvailable() {
        return isLunchAvailable;
    }

    public void setIsLunchAvailable(boolean isLunchAvailable) {
        this.isLunchAvailable = isLunchAvailable;
    }

    public boolean isDinnerAvailable() {
        return isDinnerAvailable;
    }

    public void setIsDinnerAvailable(boolean isDinnerAvailable) {
        this.isDinnerAvailable = isDinnerAvailable;
    }

    public boolean isDeliveryAvailable() {
        return isDeliveryAvailable;
    }

    public void setIsDeliveryAvailable(boolean isDeliveryAvailable) {
        this.isDeliveryAvailable = isDeliveryAvailable;
    }

    public boolean isCateringAvailable() {
        return isCateringAvailable;
    }

    public void setIsCateringAvailable(boolean isCateringAvailable) {
        this.isCateringAvailable = isCateringAvailable;
    }

    public String getChefName() {
        return chefName;
    }

    public void setChefName(String chefName) {
        this.chefName = chefName;
    }

    public String getAboutEatery() {
        return aboutEatery;
    }

    public void setAboutEatery(String aboutEatery) {
        this.aboutEatery = aboutEatery;
    }

    public int getCostForTwo() {
        return costForTwo;
    }

    public void setCostForTwo(int costForTwo) {
        this.costForTwo = costForTwo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
