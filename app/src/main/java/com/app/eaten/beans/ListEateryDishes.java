package com.app.eaten.beans;

import java.util.ArrayList;

/**
 * Created by Tushar Velingkar on 18/1/16.
 */
public class ListEateryDishes {
    private ArrayList<EateryDish> eateryDishes = new ArrayList<>();

    public ArrayList<EateryDish> getEateryDishes() {
        return eateryDishes;
    }

    public void setEateryDishes(ArrayList<EateryDish> eateryDishes) {
        this.eateryDishes = eateryDishes;
    }
}
