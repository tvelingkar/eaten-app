package com.app.eaten.eatery;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.*;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.eaten.CommonActivity;
import com.app.eaten.R;
import com.app.eaten.adapter.FragmentPagerAdapter;
import com.app.eaten.beans.CommonTypeObject;
import com.app.eaten.beans.EateryDish;
import com.app.eaten.beans.EateryObject;
import com.app.eaten.beans.EateryTime;
import com.app.eaten.beans.ListEateryDishes;
import com.app.eaten.beans.ReviewItem;
import com.app.eaten.constants.AppConstant;
import com.app.eaten.eventbus.EventBusListener;
import com.app.eaten.fragments.CuEateryTypeFragment;
import com.app.eaten.fragments.EateryReviewFragment;
import com.app.eaten.home.IHandleActivityResult;
import com.app.eaten.util.CuisineTypeOptions;
import com.app.eaten.util.EatenConstants;
import com.app.eaten.util.EateryDishesList;
import com.app.eaten.util.EateryImagesList;
import com.app.eaten.util.EateryTimeList;
import com.app.eaten.util.EateryTypeOptions;
import com.app.eaten.util.ReviewItemList;
import com.app.eaten.util.Util;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class EateryFragment extends Fragment implements EatenConstants {

    private View eateryView;
    private ViewPager mPager;
    //private PagerTabStrip mPageStrip;
    private TabLayout tabLayout;
    private FragmentPagerAdapter pagerAdapter;
    private ArrayList<Fragment> list = new ArrayList<>();
    private ArrayList<String> heading = new ArrayList<>();

    public EateryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        eateryView = inflater.inflate(R.layout.fragment_eatery, container, false);
        sendEateryRequest();
        setupViewPager();
        registerForEventBus();
        return eateryView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (menu != null) {
            android.view.MenuItem menuItem = menu.findItem(R.id.map_view);
            if (menuItem != null) {
                menuItem.setVisible(false);
            }
            menuItem = menu.findItem(R.id.grid_view);
            if (menuItem != null) {
                menuItem.setVisible(false);
            }
        }
    }

    private void setupViewPager() {
        mPager = (ViewPager) eateryView.findViewById(R.id.pager);
        //mPageStrip = (PagerTabStrip) eateryView.findViewById(R.id.pager_title_strip);
        tabLayout = (TabLayout) eateryView.findViewById(R.id.tabs);
        tabLayout.setTabTextColors(ContextCompat.getColor(getContext(), R.color.hintPrimary), ContextCompat.getColor(getContext(), R.color.textPrimary));

        if (Util.getPrefs(getActivity()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
            if (Util.getPrefs(getActivity()).getBoolean(IS_EATERY_ACTIVATED, false)) {
                Util.getPrefs(getActivity()).edit().putBoolean(IS_TEMP_USER, true).apply();
                list.add(new GeneralInfoFragment());
                heading.add("General Info");
                list.add(new CuEateryTypeFragment());
                heading.add("Eatery Info");
                list.add(new GalleryMenuFragment());
                heading.add("Menu Info");
                list.add(new EateryReviewFragment());
                heading.add("Reviews");
            } else {
                if (OPTION_YES.equals(Util.getPrefs(getContext()).getString(IS_EATERY_CREATED, OPTION_NO))) {
                    list.add(new GalleryMenuFragment());
                    heading.add("Menu Info");
                } else {
                    list.add(new GeneralInfoFragment());
                    heading.add("General Info");
                    list.add(new EateryTypeFragment());
                    heading.add("Eatery Info");
                }
            }
        } else if (Util.getPrefs(getActivity()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == CUSTOMER_SELECTION) {
            list.add(new GeneralInfoFragment());
            heading.add("General Info");
            list.add(new CuEateryTypeFragment());
            heading.add("Eatery Info");
            list.add(new GalleryMenuFragment());
            heading.add("Menu Info");
            list.add(new EateryReviewFragment());
            heading.add("Reviews");
        }

        pagerAdapter = new FragmentPagerAdapter(getChildFragmentManager(), list, heading);
        mPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(mPager);
    }

    public void onEvent(EventBusListener mEventListener) {
        switch(mEventListener.getType()) {
            case "NAVIGATE_TO_EATERY_INFO_FRAGMENT" :
                mPager.setCurrentItem(1, true);
                break;
            case "ADD_GALLERY_FRAGMENT" :
                Log.v("Eaten", "Inside Add Gallery Fragment");
                list.clear();
                heading.clear();
                list.add(new GalleryMenuFragment());
                heading.add("Menu Info");
                pagerAdapter = new FragmentPagerAdapter(getChildFragmentManager(), list, heading);
                mPager.setAdapter(pagerAdapter);
                tabLayout.setupWithViewPager(mPager);
                break;
            case "NAVIGATE_BACK_DATA_INCOMPLETE" :
                mPager.setCurrentItem(0, true);
                final AlertDialog builder = new AlertDialog.Builder(getContext()).create();
                builder.setTitle("Some Fields are Empty");
                builder.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        builder.dismiss();
                    }
                });
                builder.show();
                break;
            case EDIT_EATERY_FLAG :
                Util.getPrefs(getActivity()).edit().putBoolean(IS_TEMP_USER, false).apply();
                list.clear();
                heading.clear();
                list.add(new GeneralInfoFragment());
                heading.add("General Info");
                list.add(new EateryTypeFragment());
                heading.add("Eatery Info");
                list.add(new GalleryMenuFragment());
                heading.add("Menu Info");
                list.add(new EateryReviewFragment());
                heading.add("Reviews");
                pagerAdapter = new FragmentPagerAdapter(getChildFragmentManager(), list, heading);
                mPager.setAdapter(pagerAdapter);
                tabLayout.setupWithViewPager(mPager);
                break;
            case ACTIVATE_EATERY_FLAG :
                Util.getPrefs(getActivity()).edit().putBoolean(IS_TEMP_USER, true).apply();
                list.clear();
                heading.clear();
                list.add(new GeneralInfoFragment());
                heading.add("General Info");
                list.add(new CuEateryTypeFragment());
                heading.add("Eatery Info");
                list.add(new GalleryMenuFragment());
                heading.add("Menu Info");
                list.add(new EateryReviewFragment());
                heading.add("Reviews");
                pagerAdapter = new FragmentPagerAdapter(getChildFragmentManager(), list, heading);
                mPager.setAdapter(pagerAdapter);
                tabLayout.setupWithViewPager(mPager);
                break;
        }

    }

    private void registerForEventBus() {

        try {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);

        } catch (ClassCastException e) {
            e.printStackTrace();
        }

    }

    private void sendEateryRequest(){

        String url = null;
        if (Util.getPrefs(getActivity()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == CUSTOMER_SELECTION) {
            url = String.format("%s%s?uniqueId=%s", SERVER_BASE_URL, Util.getPrefs(getContext()).getString(EATERY_ID, ""), Util.getPrefs(getContext()).getString(LOGIN_UNIQUE_ID, ""));
        } else if (Util.getPrefs(getContext()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
            if (Util.getPrefs(getActivity()).getBoolean(IS_EATERY_ACTIVATED, false)) {
                url = String.format("%s%s?uniqueId=%s", SERVER_BASE_URL, Util.getPrefs(getContext()).getString(EATERY_ID, ""), Util.getPrefs(getContext()).getString(LOGIN_UNIQUE_ID, ""));
            }
        }

        if (url != null) {
            Log.v("Test", "URl -- " + url);
            JsonObjectRequest myEateryRequest = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.optString("status").isEmpty()) {
                            Gson gson = new Gson();
                            String json = Util.getPrefs(getContext()).getString(EATERY_OBJECT, "");
                            EateryObject eateryObject = gson.fromJson(json, EateryObject.class);
                            if (eateryObject == null) {
                                eateryObject = new EateryObject();
                            }
                            if (Util.getPrefs(getActivity()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == CUSTOMER_SELECTION) {
                                eateryObject.setEateryName(response.optString("eateryName"));
                                eateryObject.setYearOfEstablishment(response.optString("establishedYear"));
                                eateryObject.setAddress(response.optString("firstAddress"));
                                eateryObject.setMobileNo(response.optString("mobile"));
                                if ("Y".equals(response.optString("breakfast"))) {
                                    eateryObject.setIsBreakfastAvailable(true);
                                }
                                if ("Y".equals(response.optString("lunch"))) {
                                    eateryObject.setIsLunchAvailable(true);
                                }
                                if ("Y".equals(response.optString("dinner"))) {
                                    eateryObject.setIsDinnerAvailable(true);
                                }
                                if ("Y".equals(response.optString("delivery"))) {
                                    eateryObject.setIsDeliveryAvailable(true);
                                }
                                if ("Y".equals(response.optString("cateringAvailable"))) {
                                    eateryObject.setIsCateringAvailable(true);
                                }
                                eateryObject.setEateryLogo(response.optString("EateryLogo"));

                                JSONArray objectType = response.optJSONArray("eateryType");
                                int count;
                                int index;
                                JSONObject object;
                                String value = "";
                                if (objectType != null) {
                                    count = objectType.length();
                                    for (index = 0; index < count; index++) {
                                        object = objectType.optJSONObject(index);
                                        if (object != null) {
                                            value += object.optString("name") + ", ";
                                        }
                                    }
                                }
                                eateryObject.setEateryType(value);

                                objectType = response.optJSONArray("cuisineType");
                                value = "";
                                if (objectType != null) {
                                    count = objectType.length();
                                    for (index = 0; index < count; index++) {
                                        object = objectType.optJSONObject(index);
                                        if (object != null) {
                                            value += object.optString("name") + ", ";
                                        }
                                    }
                                }
                                eateryObject.setCuisineType(value);

                                eateryObject.setPaymentType(response.optString("payment"));
                                eateryObject.setChefName(response.optString("chefName"));
                                eateryObject.setCostForTwo(response.optInt("costForTwo"));
                                json = gson.toJson(eateryObject);
                                Util.getPrefs(getContext()).edit().putString(EATERY_OBJECT, json).apply();

                                JSONArray imagesUrl = response.optJSONArray("EateryImages");
                                if (imagesUrl != null) {
                                    EateryImagesList.reset();
                                    int size = imagesUrl.length();
                                    int i = 0;
                                    JSONObject image;
                                    for (; i < size; i++) {
                                        image = imagesUrl.optJSONObject(i);
                                        if (image != null) {
                                            EateryImagesList.addEntry(image.optInt("id"), image.optString("path"));
                                        }
                                    }
                                }

                                JSONArray eateryDishesJSON = response.optJSONArray("EateryDishes");
                                ListEateryDishes eateryDishes = gson.fromJson(json, ListEateryDishes.class);
                                int key;
                                if (eateryDishesJSON != null) {
                                    eateryDishes = new ListEateryDishes();
                                    EateryDishesList.reset();
                                    int size = eateryDishesJSON.length();
                                    int i = 0;
                                    JSONObject dish;
                                    EateryDish eateryDish;
                                    for (; i < size; i++) {
                                        dish = eateryDishesJSON.optJSONObject(i);
                                        if (dish != null) {
                                            key = eateryDishes.getEateryDishes().size();
                                            eateryDish = new EateryDish();
                                            eateryDish.imageId = dish.optInt("id");
                                            eateryDish.dishName = dish.optString("dishName");
                                            eateryDish.dishPrice = dish.optInt("dishPrice");
                                            eateryDish.imageUrl = dish.optString("image");
                                            eateryDishes.getEateryDishes().add(key, eateryDish);
                                            EateryDishesList.addEntry(eateryDish);
                                        }
                                    }
                                    json = gson.toJson(eateryDishes);
                                    Util.getPrefs(getContext()).edit().putString(EATERY_DISH_LIST_OBJECT, json).apply();
                                }
                                JSONArray eateryTime = response.optJSONArray("EateryTime");
                                if (eateryTime != null) {
                                    EateryTimeList.reset();
                                    int size = eateryTime.length();
                                    int i = 0;
                                    JSONObject time;
                                    EateryTime timeObject;
                                    for (; i < size; i++) {
                                        time = eateryTime.optJSONObject(i);
                                        if (time != null) {
                                            timeObject = new EateryTime();
                                            timeObject.day = time.optString("day");
                                            timeObject.breakfast = time.optString("breakfast");
                                            timeObject.lunch = time.optString("lunch");
                                            timeObject.dinner = time.optString("dinner");
                                            EateryTimeList.addEntry(time.optInt("id"), timeObject);
                                        }
                                    }
                                }
                            }
                            JSONArray reviewList = response.optJSONArray("review");
                            if (reviewList != null) {
                                ReviewItemList.reset();
                                int size = reviewList.length();
                                int i = 0;
                                JSONObject review;
                                ReviewItem reviewItem;
                                for (; i < size; i++) {
                                    review = reviewList.optJSONObject(i);
                                    if (review != null) {
                                        reviewItem = new ReviewItem();
                                        reviewItem.reviewId = review.optInt("id");
                                        reviewItem.userId = review.optString("userId");
                                        reviewItem.reviewRating = review.optInt("rating");
                                        reviewItem.reviewContent = review.optString("review");
                                        reviewItem.reviewDate = review.optString("date");
                                        ReviewItemList.addEntry(reviewItem);
                                    }
                                }
                            }
                            EventBusListener eventBusListener = new EventBusListener();
                            eventBusListener.setType(AppConstant.EVENT_EATERY_DATA_FETCHED);
                            EventBus.getDefault().post(eventBusListener);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(), "Error while retrieving my eatery!!!", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
            };

            ((CommonActivity) getActivity()).addRequestToQueue(myEateryRequest);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v("Eaten", "Request Code "+requestCode);
        super.onActivityResult(requestCode, resultCode, data);
        handleResult(this, requestCode, resultCode, data);
    }

    private void handleResult(Fragment frag, int requestCode, int resultCode, Intent data) {
        if (frag instanceof IHandleActivityResult) { // custom interface with no signitures
            frag.onActivityResult(requestCode, resultCode, data);
        }
        List<Fragment> frags = frag.getChildFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment f : frags) {
                if (f != null)
                    handleResult(f, requestCode, resultCode, data);
            }
        }
    }
}
