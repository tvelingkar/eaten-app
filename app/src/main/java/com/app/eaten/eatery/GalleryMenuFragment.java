package com.app.eaten.eatery;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.eaten.CommonActivity;
import com.app.eaten.R;
import com.app.eaten.adapter.CommonAdapter;
import com.app.eaten.adapter.PopulationListener;
import com.app.eaten.beans.EateryDish;
import com.app.eaten.beans.ListEateryDishes;
import com.app.eaten.constants.AppConstant;
import com.app.eaten.eventbus.EventBusListener;
import com.app.eaten.home.IHandleActivityResult;
import com.app.eaten.util.EatenConstants;
import com.app.eaten.util.ExpandableHeightGridView;
import com.app.eaten.util.UploadFileToServer;
import com.app.eaten.util.Util;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryMenuFragment extends Fragment implements EatenConstants, IHandleActivityResult {

    private View galleryMenuView;
    private LayoutInflater mInflater;
    private AlertDialog.Builder builder;
    private CommonAdapter<EateryDish> commonAdapter;
    private AlertDialog imagePopup;
    private String fileName;
    private File file;
    private Uri imageURI;
    private ImageView imageView;
    private boolean isLogoUpload;

    public GalleryMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        galleryMenuView = inflater.inflate(R.layout.fragment_gallery_menu, container, false);
        setupViews();
        registerForEventBus();
        loadImages();
        return galleryMenuView;
    }

    private void setupViews() {

        mInflater = LayoutInflater.from(getActivity());

        if (Util.getPrefs(getActivity()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
            if (!Util.getPrefs(getActivity()).getBoolean(IS_TEMP_USER, false)) {
                galleryMenuView.findViewById(R.id.add_menu_container).setVisibility(View.VISIBLE);
                galleryMenuView.findViewById(R.id.add_logo_container).setVisibility(View.VISIBLE);

                final Button btnActivate = (Button) galleryMenuView.findViewById(R.id.btnActivate);
                btnActivate.setVisibility(View.VISIBLE);
                btnActivate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if ("Close".equals(btnActivate.getText())) {
                            EventBusListener eventBusListener = new EventBusListener();
                            eventBusListener.setType(ACTIVATE_EATERY_FLAG);
                            EventBus.getDefault().post(eventBusListener);
                        } else {
                            if ("".equals(Util.getPrefs(getContext()).getString(EATERY_LOGO, ""))) {
                                Toast.makeText(getContext(), "Upload Eatery Logo to activate the Eatery", Toast.LENGTH_LONG).show();
                            } else {
                                EventBusListener eventBusListener = new EventBusListener();
                                eventBusListener.setType(ACTIVATE_EATERY_FLAG);
                                EventBus.getDefault().post(eventBusListener);
                            }
                        }
                    }
                });

                Button addLogo = (Button) galleryMenuView.findViewById(R.id.addLogo);
                addLogo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        imageView = (ImageView) galleryMenuView.findViewById(R.id.eateryLogoImg);
                        isLogoUpload = true;
                        selectImage();
                    }
                });

                if (!"".equals(Util.getPrefs(getContext()).getString(EATERY_LOGO, ""))) {
                    btnActivate.setText("Close");
                    addLogo.setText("Change Logo");
                    galleryMenuView.findViewById(R.id.logo_container).setVisibility(View.VISIBLE);
                    imageView = (ImageView) galleryMenuView.findViewById(R.id.eateryLogoImg);
                    Picasso.with(getActivity()).load(Util.getPrefs(getContext()).getString(EATERY_LOGO, ""))
                            .placeholder(ContextCompat.getDrawable(getContext(), R.drawable.add_photo))
                            .fit()
                            .into(imageView);
                }

                Button addMenu = (Button) galleryMenuView.findViewById(R.id.addMenu);
                addMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        View addItemView = mInflater.inflate(R.layout.add_menu, null);
                        final EditText itemName = (EditText) addItemView.findViewById(R.id.itemName);
                        final EditText itemPrice = (EditText) addItemView.findViewById(R.id.itemPrice);
                        Button addItemBtn = (Button) addItemView.findViewById(R.id.addItemBtn);
                        final ImageView itemPic = (ImageView) addItemView.findViewById(R.id.itemPic);

                        itemPic.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                imageView = itemPic;
                                selectImage();
                            }
                        });

                        addItemBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if ((itemName.getText() == null || itemName.getText().toString().isEmpty()) && (itemPrice.getText() == null || itemPrice.getText().toString().isEmpty())) {
                                    Toast.makeText(getContext(), "Please fill all the fields", Toast.LENGTH_LONG).show();
                                } else {
                                    addMenu(itemName.getText().toString(), itemPrice.getText().toString());
                                }
                            }
                        });

                        builder.setView(addItemView);
                        imagePopup = builder.show();
                    }
                });
            }
        }
        builder = new AlertDialog.Builder(getActivity());
        ExpandableHeightGridView menuGrid = (ExpandableHeightGridView) galleryMenuView.findViewById(R.id.menuGridView);
        menuGrid.setExpanded(true);

        String json = Util.getPrefs(getContext()).getString(EATERY_DISH_LIST_OBJECT, "");
        Gson gson = new Gson();
        ListEateryDishes eateryDishes = gson.fromJson(json, ListEateryDishes.class);
        if (eateryDishes == null) {
            eateryDishes = new ListEateryDishes();
        }
        commonAdapter = new CommonAdapter<>(getActivity(), eateryDishes.getEateryDishes());
        int[] viewIDs = {R.id.menuImg, R.id.menuName, R.id.menuPrice, R.id.editMenu, R.id.deleteMenu};
        commonAdapter.setLayoutTextViews(R.layout.menu_grid_item, viewIDs);
        commonAdapter.setPopulationListener(new PopulationListener<EateryDish>() {
            @Override
            public void populateFrom(View v, int position, final EateryDish row, View[] views) {

                if (Util.getPrefs(getContext()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
                    if (Util.getPrefs(getActivity()).getBoolean(IS_TEMP_USER, false)) {
                        Picasso.with(getActivity()).load(SERVER_BASE_URL + row.imageUrl + "?uniqueId=" + Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, ""))
                                .placeholder(ContextCompat.getDrawable(getContext(), R.drawable.add_photo))
                                .fit()
                                .into(((ImageView) views[0]));
                        (views[3]).setVisibility(View.GONE);
                        (views[4]).setVisibility(View.GONE);
                    } else {
                        if (row.imageLocalUri != null) {
                            // File is  present in Local Storage
                            Picasso.with(getActivity()).load(row.imageLocalUri)
                                    .placeholder(ContextCompat.getDrawable(getContext(), R.drawable.add_photo))
                                    .fit()
                                    .into(((ImageView) views[0]));
                        } else {
                            Picasso.with(getActivity()).load(SERVER_BASE_URL + row.imageUrl + "?uniqueId=" + Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, ""))
                                    .placeholder(ContextCompat.getDrawable(getContext(), R.drawable.add_photo))
                                    .fit()
                                    .into(((ImageView) views[0]));
                        }
                    }
                } else {
                    Picasso.with(getActivity()).load(SERVER_BASE_URL + row.imageUrl + "?uniqueId=" + Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, ""))
                            .placeholder(ContextCompat.getDrawable(getContext(), R.drawable.add_photo))
                            .fit()
                            .into(((ImageView) views[0]));
                    (views[3]).setVisibility(View.GONE);
                    (views[4]).setVisibility(View.GONE);
                }
                ((TextView) views[1]).setText(row.dishName);
                ((TextView) views[2]).setText(String.valueOf(row.dishPrice));

                (views[3]).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        View addItemView = mInflater.inflate(R.layout.add_menu, null);
                        final EditText itemName = (EditText) addItemView.findViewById(R.id.itemName);
                        final EditText itemPrice = (EditText) addItemView.findViewById(R.id.itemPrice);
                        Button addItemBtn = (Button) addItemView.findViewById(R.id.addItemBtn);
                        final ImageView itemPic = (ImageView) addItemView.findViewById(R.id.itemPic);

                        itemName.setText(row.dishName);
                        itemPrice.setText(String.valueOf(row.dishPrice));
                        if (Util.getPrefs(getContext()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
                            if (Util.getPrefs(getActivity()).getBoolean(IS_TEMP_USER, false)) {
                                Picasso.with(getActivity()).load(SERVER_BASE_URL + row.imageUrl + "?uniqueId=" + Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, ""))
                                        .placeholder(ContextCompat.getDrawable(getContext(), R.drawable.add_photo))
                                        .fit()
                                        .into(itemPic);
                            } else {
                                if (AppConstant.getPath(Uri.parse(row.imageLocalUri), getContext()) != null) {
                                    // File is  present in Local Storage
                                    Picasso.with(getActivity()).load(row.imageLocalUri)
                                            .placeholder(ContextCompat.getDrawable(getContext(), R.drawable.add_photo))
                                            .fit()
                                            .into(itemPic);
                                } else {
                                    Picasso.with(getActivity()).load(SERVER_BASE_URL + row.imageUrl + "?uniqueId=" + Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, ""))
                                            .placeholder(ContextCompat.getDrawable(getContext(), R.drawable.add_photo))
                                            .fit()
                                            .into(itemPic);
                                }
                            }
                        } else {
                            Picasso.with(getActivity()).load(SERVER_BASE_URL + row.imageUrl + "?uniqueId=" + Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, ""))
                                    .placeholder(ContextCompat.getDrawable(getContext(), R.drawable.add_photo))
                                    .fit()
                                    .into(itemPic);
                        }


                        itemPic.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                imageView = itemPic;
                                selectImage();
                            }
                        });

                        addItemBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                updateMenu(itemName.getText().toString(), itemPrice.getText().toString(), row.imageId);
                            }
                        });

                        builder.setView(addItemView);
                        imagePopup = builder.show();

                    }
                });

                (views[4]).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteMenuItemPicRequest(row.imageId);
                    }
                });

            }

            @Override
            public void onRowCreate(View[] views) {

            }
        });

        menuGrid.setAdapter(commonAdapter);
    }

    private void deleteMenuItemPicRequest(final int menuID) {

        String url = "?uniqueId=" + Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, "") +
                "&eateryId=" + Util.getPrefs(getActivity()).getString(EATERY_ID, "") + "&dishId=" + menuID;

        JsonObjectRequest updateMenuRequest = new JsonObjectRequest(Request.Method.POST, SERVER_BASE_URL +
                DELETE_EATERY_IMAGE_URL + url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getActivity(), "Successfully deleted menu image!!!", Toast.LENGTH_SHORT).show();
                Log.d("TAG", "response --- " + response.toString());
                deleteMenuItemDishRequest(menuID);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Error while deleting menu item pic!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        ((CommonActivity) getActivity()).addRequestToQueue(updateMenuRequest);

    }

    private void deleteMenuItemDishRequest(int menuID) {

        String url = "?uniqueId=" + Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, "") +
                "&eateryId=" + Util.getPrefs(getActivity()).getString(EATERY_ID, "") + "&dishId=" + menuID;

        JsonObjectRequest updateMenuRequest = new JsonObjectRequest(Request.Method.POST, SERVER_BASE_URL +
                DELETE_EATERY_DISH_URL + url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getActivity(), "Successfully deleted menu !!!", Toast.LENGTH_SHORT).show();
                Log.d("TAG", "response --- " + response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Error while deleting menu item !!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        ((CommonActivity) getActivity()).addRequestToQueue(updateMenuRequest);

    }

    public void onEvent(EventBusListener mEventListener) {
        switch (mEventListener.getType()) {
            case "MENU_UPLOADED":
                imagePopup.dismiss();
                loadImages();
                break;
            case "LOGO_UPLOADED":
                Util.getPrefs(getParentFragment().getActivity()).edit().putBoolean(IS_EATERY_ACTIVATED, true).apply();
                Util.getPrefs(getParentFragment().getActivity()).edit().putBoolean(IS_TEMP_USER, true).apply();
                break;
        }
    }

    private void loadImages() {
        String json = Util.getPrefs(getContext()).getString(EATERY_DISH_LIST_OBJECT, "");
        Gson gson = new Gson();
        ListEateryDishes eateryDishes = gson.fromJson(json, ListEateryDishes.class);
        if (eateryDishes == null) {
            eateryDishes = new ListEateryDishes();
        }
        commonAdapter.swapItems(eateryDishes.getEateryDishes());
    }

    private void registerForEventBus() {

        try {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);

        } catch (ClassCastException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    fileName = "eatery_" + timeStamp + ".jpg";
                    File f = new File(EATERY_IMAGES_PUBLIC + fileName);
                    imageURI = Uri.fromFile(f);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                    dialog.dismiss();
                    getParentFragment().startActivityForResult(intent, TAKE_PIC_REQ_CODE);
                    // dispatchTakePictureIntent();
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    dialog.dismiss();
                    getParentFragment().startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            CHOOSE_LIB_REQ_CODE);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void addMenu(String itemName, String itemPrice) {

        try {
            if (file != null) {

                UploadFileToServer uploadFileToServer = new UploadFileToServer(file, getContext(), UPLOAD_EATERY_DISH_URL, UPLOAD_EATERY_DISH);
                uploadFileToServer.execute(itemName, itemPrice, imageURI.toString());

            } else {
                Toast.makeText(getContext(), "Please select an Item image!!!", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addLogo() {
        galleryMenuView.findViewById(R.id.logo_container).setVisibility(View.VISIBLE);
        try {
            if (file != null) {
                if (file.exists()) {
                    Log.v("Eaten", "Inside Logo");
                    Util.getPrefs(getContext()).edit().putString(EATERY_LOGO, imageURI.toString()).apply();
                    UploadFileToServer uploadFileToServer = new UploadFileToServer(file, getContext(), UPLOAD_EATERY_LOGO_URL, UPLOAD_EATERY_LOGO);
                    uploadFileToServer.execute();
                }

            } else {
                Toast.makeText(getContext(), "Please select a Logo image!!!", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        isLogoUpload = false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PIC_REQ_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    File f = Environment.getExternalStorageDirectory();
                    for (File temp : f.listFiles()) {
                        if (temp.getName().equals("temp.jpg")) {
                            f = temp;
                            break;
                        }
                    }
                    try {
                        Bitmap bm;

                        fileName = f.getAbsolutePath();
                        bm = Util.decodeSampledBitmapFromPath(fileName, 100, 100);

                        file = getFileFromBitmap(bm);
                        imageView.setImageBitmap(bm);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (isLogoUpload) {
                        addLogo();
                    }
                }
                break;

            case CHOOSE_LIB_REQ_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    imageURI = data.getData();

                    String tempPath = AppConstant.getPath(imageURI, getContext());

                    fileName = tempPath;
                    Bitmap bm;

                    bm = Util.decodeSampledBitmapFromPath(tempPath, 100, 100);
                    file = getFileFromBitmap(bm);
                    imageView.setImageBitmap(bm);
                    if (isLogoUpload) {
                        addLogo();
                    }

                }
                break;
        }
    }

    private File getFileFromBitmap(Bitmap bitmap) {
        try {
            //create a file to write bitmap data
            File f = new File(fileName);
            f.createNewFile();

//Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

            return f;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateMenu(String itemName, String itemPrice, int dishID) {

        try {
            String url = "?uniqueId=" + Util.getPrefs(getContext()).getString(LOGIN_UNIQUE_ID, "") +
                    "&eateryId=" + Util.getPrefs(getContext()).getString(EATERY_ID, "") + "&dishId=" + dishID +
                    "&dishName=" + itemName + "&dishPrice=" + itemPrice;

            JsonObjectRequest updateMenuRequest = new JsonObjectRequest(Request.Method.POST, SERVER_BASE_URL +
                    UPDATE_EATERY_DISH_URL + url, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(getContext(), "Successfully updated menu!!!", Toast.LENGTH_SHORT).show();
                    Log.d("TAG", "response --- " + response.toString());


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getContext(), "Error while updating eatery!!!", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
            };

            ((CommonActivity) getActivity()).addRequestToQueue(updateMenuRequest);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
