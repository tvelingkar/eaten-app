package com.app.eaten.eatery;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.app.eaten.R;
import com.app.eaten.activities.MapSelectionActivity;
import com.app.eaten.adapter.FilterWithSpaceAdapter;
import com.app.eaten.beans.EateryObject;
import com.app.eaten.constants.AppConstant;
import com.app.eaten.eventbus.EventBusListener;
import com.app.eaten.fragments.YearPickerFragment;
import com.app.eaten.home.IHandleActivityResult;
import com.app.eaten.services.PlaceJSONParser;
import com.app.eaten.util.EatenConstants;
import com.app.eaten.util.UploadFileToServer;
import com.app.eaten.util.Util;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class GeneralInfoFragment extends Fragment implements EatenConstants {

    View generalInfoView;
    Button continueBtn;
    EditText owEateryName, owEateryYear, owEateryArea, owEateryCity, owEateryState, owEateryCountry, owEateryMobile;
    AppCompatTextView cuEateryName, cuEateryYear, cuEateryAddress, cuEateryArea, cuEateryCity, cuEateryState, cuEateryCountry, cuEateryMobile;
    AppCompatCheckBox bfCheck, lunchCheck, dinnerCheck;
    AppCompatAutoCompleteTextView owEateryAddress;

    String eName, eYear, eAddress, eArea, eCity, eState, eCountry, ePincode, eMobile;
    boolean isBFAvaialble, isLunchAvailable, isDinnerAvailable;

    private PlacesTask placesTask;
    private String key;
    private Location mLastLocation;
    private CircleImageView eateryLogo;
    private String fileName;
    private String path;

    public GeneralInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        generalInfoView = inflater.inflate(R.layout.fragment_general_info, container, false);
        if (Util.getPrefs(getActivity()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
            if (Util.getPrefs(getActivity()).getBoolean(IS_TEMP_USER, false)) {
                setupCustomerViews();
            } else {
                setupOwnerViews();
            }
        } else if (Util.getPrefs(getActivity()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == CUSTOMER_SELECTION) {
            setupCustomerViews();
        }
        registerForEventBus();
        setupUIfromLocal();
        return generalInfoView;
    }

    private void setupOwnerViews() {
        generalInfoView.findViewById(R.id.eateryOwnerView).setVisibility(View.VISIBLE);
        owEateryName = (EditText) generalInfoView.findViewById(R.id.eateryNameEditTxt);
        owEateryYear = (EditText) generalInfoView.findViewById(R.id.yearEditTxt);
        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                owEateryYear.setText(String.format("%s", year));
            }
        };
        final YearPickerFragment yearPickerFragment = new YearPickerFragment();
        yearPickerFragment.setDateListener(onDateSetListener);
        owEateryYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yearPickerFragment.show(getFragmentManager().beginTransaction(), "datePicker");
            }
        });
        owEateryAddress = (AppCompatAutoCompleteTextView) generalInfoView.findViewById(R.id.addressEditTxt);
        owEateryAddress.setThreshold(1);

        // Adding textchange listener
        owEateryAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (placesTask != null) {
                    placesTask.cancel(true);
                }
                placesTask = new PlacesTask();
                String loc = s.toString();
                loc = loc.trim();
                loc = loc.replaceAll(" ", "+");
                placesTask.execute(loc);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
        /*owEateryArea = (EditText) generalInfoView.findViewById(R.id.areaEditTxt);
        owEateryCity = (EditText) generalInfoView.findViewById(R.id.cityEditTxt);
        owEateryState = (EditText) generalInfoView.findViewById(R.id.stateEditTxt);
        owEateryCountry = (EditText) generalInfoView.findViewById(R.id.countryEditTxt);*/
        owEateryMobile = (EditText) generalInfoView.findViewById(R.id.mobileNumEditTxt);
        continueBtn = (Button) generalInfoView.findViewById(R.id.continueBtn);
        bfCheck = (AppCompatCheckBox) generalInfoView.findViewById(R.id.bfAvaialbleToggle);
        lunchCheck = (AppCompatCheckBox) generalInfoView.findViewById(R.id.lunchAvaialbleToggle);
        dinnerCheck = (AppCompatCheckBox) generalInfoView.findViewById(R.id.dinnerAvaialbleToggle);

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eName = owEateryName.getText().toString();
                eYear = owEateryYear.getText().toString();
                eAddress = owEateryAddress.getText().toString();
                /*eArea = owEateryArea.getText().toString();
                eCity = owEateryCity.getText().toString();
                eState = owEateryState.getText().toString();
                eCountry = owEateryCountry.getText().toString();*/
                eMobile = owEateryMobile.getText().toString();
                isBFAvaialble = bfCheck.isChecked();
                isLunchAvailable = lunchCheck.isChecked();
                isDinnerAvailable = dinnerCheck.isChecked();

                String json = Util.getPrefs(getParentFragment().getActivity()).getString(EATERY_OBJECT, "");
                Gson gson = new Gson();
                EateryObject eateryObject = gson.fromJson(json, EateryObject.class);
                if (eateryObject == null) {
                    eateryObject = new EateryObject();
                }
                eateryObject.setEateryName(eName);
                eateryObject.setYearOfEstablishment(eYear);
                eateryObject.setAddress(eAddress);
                eateryObject.setMobileNo(eMobile);
                eateryObject.setIsBreakfastAvailable(isBFAvaialble);
                eateryObject.setIsLunchAvailable(isLunchAvailable);
                eateryObject.setIsDinnerAvailable(isDinnerAvailable);

                json = gson.toJson(eateryObject);
                Util.getPrefs(getContext()).edit().putString(EATERY_OBJECT, json).apply();

                if (eName.isEmpty() || eYear.isEmpty() || eAddress.isEmpty() || eMobile.isEmpty()) {
                    final AlertDialog builder = new AlertDialog.Builder(getParentFragment().getActivity()).create();
                    builder.setTitle("Some Fields are Empty");
                    builder.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            builder.dismiss();
                        }
                    });
                    builder.show();
                } else {
                    EventBusListener eventbusListener = new EventBusListener();
                    eventbusListener.setType("NAVIGATE_TO_EATERY_INFO_FRAGMENT");
                    EventBus.getDefault().post(eventbusListener);
                }

            }
        });
        generalInfoView.findViewById(R.id.getLocation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MapSelectionActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setupCustomerViews() {
        generalInfoView.findViewById(R.id.customerView).setVisibility(View.VISIBLE);
        cuEateryName = (AppCompatTextView) generalInfoView.findViewById(R.id.cuEateryNameTxt);
        cuEateryYear = (AppCompatTextView) generalInfoView.findViewById(R.id.cuYearTxt);
        cuEateryAddress = (AppCompatTextView) generalInfoView.findViewById(R.id.cuAddressTxt);
        /*cuEateryArea = (TextView) generalInfoView.findViewById(R.id.areaTxt);
        cuEateryCity = (TextView) generalInfoView.findViewById(R.id.cityTxt);
        cuEateryState = (TextView) generalInfoView.findViewById(R.id.stateTxt);
        cuEateryCountry = (TextView) generalInfoView.findViewById(R.id.countryTxt);*/
        cuEateryMobile = (AppCompatTextView) generalInfoView.findViewById(R.id.cuMobileNumTxt);
        bfCheck = (AppCompatCheckBox) generalInfoView.findViewById(R.id.cuBFAvaialble);
        lunchCheck = (AppCompatCheckBox) generalInfoView.findViewById(R.id.cuLunchAvaialble);
        dinnerCheck = (AppCompatCheckBox) generalInfoView.findViewById(R.id.cuDinnerAvaialble);
        eateryLogo = (CircleImageView) generalInfoView.findViewById(R.id.eateryLogo);
    }

    public void onEvent(EventBusListener mEventListener) {
        switch (mEventListener.getType()) {
            case AppConstant.EVENT_EATERY_LOC_SELECTED:
                Address eateryAddress = (Address) mEventListener.getData();
                Log.v("Test", "Location received");
                if (eateryAddress != null) {
                    Log.v("Test", "Location Not Null");
                    String localityString = eateryAddress.getSubLocality();
                    String city = eateryAddress.getLocality();
                    String country = eateryAddress.getCountryName();
                    String zipcode = "None";
                    if (eateryAddress.getPostalCode() != null) {
                        zipcode = eateryAddress.getPostalCode();
                    }
                    String state = eateryAddress.getAdminArea();
                    owEateryAddress.setText(String.format("%s, %s, %s, %s", localityString, city, state, country));
                    owEateryAddress.setSelection(0);
                    owEateryAddress.clearFocus();
                    Gson gson = new Gson();
                    String json = Util.getPrefs(getParentFragment().getActivity()).getString(EATERY_OBJECT, "");
                    EateryObject eateryObject = gson.fromJson(json, EateryObject.class);
                    if (eateryObject == null) {
                        eateryObject = new EateryObject();
                    }
                    eateryObject.setAddress(localityString + " " + city + " " + state + " " + country);
                    eateryObject.setLatitude(eateryAddress.getLatitude());
                    eateryObject.setLongitude(eateryAddress.getLongitude());
                    eateryObject.setArea(localityString);
                    eateryObject.setCity(city);
                    eateryObject.setCountry(country);
                    eateryObject.setPincode(zipcode);
                    eateryObject.setState(state);
                    json = gson.toJson(eateryObject);
                    Util.getPrefs(getContext()).edit().putString(EATERY_OBJECT, json).apply();
                }

                break;
            case AppConstant.EVENT_EATERY_DATA_FETCHED:
                setupUIfromLocal();
                break;
        }
    }

    private void setupUIfromLocal() {
        try {
            Gson gson = new Gson();
            String json = Util.getPrefs(getContext()).getString(EATERY_OBJECT, "");
            EateryObject obj = gson.fromJson(json, EateryObject.class);
            if (obj != null) {
                if (Util.getPrefs(getContext()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
                    if (Util.getPrefs(getContext()).getBoolean(IS_TEMP_USER, false)) {
                        cuEateryName.setText(obj.getEateryName());
                        cuEateryYear.setText(obj.getYearOfEstablishment());
                        cuEateryAddress.setText(obj.getAddress());
                        cuEateryMobile.setText(obj.getMobileNo());
                        if (obj.isBreakfastAvailable()) {
                            bfCheck.setChecked(true);
                        } else {
                            bfCheck.setChecked(false);
                        }
                        if (obj.isLunchAvailable()) {
                            lunchCheck.setChecked(true);
                        } else {
                            lunchCheck.setChecked(false);
                        }
                        if (obj.isDinnerAvailable()) {
                            dinnerCheck.setChecked(true);
                        } else {
                            dinnerCheck.setChecked(false);
                        }
                        if ("".equals(Util.getPrefs(getContext()).getString(EATERY_LOGO, ""))) {
                            Picasso.with(getContext()).load(R.drawable.add_photo)
                                    .fit()
                                    .into(eateryLogo);
                        } else {
                            Picasso.with(getContext()).load(Util.getPrefs(getContext()).getString(EATERY_LOGO, ""))
                                    .placeholder(ContextCompat.getDrawable(getContext(), R.drawable.add_photo))
                                    .fit()
                                    .into(eateryLogo);
                        }
                    } else {
                        owEateryName.setText(obj.getEateryName());
                        owEateryYear.setText(obj.getYearOfEstablishment());
                        owEateryAddress.setText(obj.getAddress());
                        owEateryAddress.setSelection(0);
                        owEateryAddress.clearFocus();
                        owEateryMobile.setText(obj.getMobileNo());
                        if (obj.isBreakfastAvailable()) {
                            bfCheck.setChecked(true);
                        } else {
                            bfCheck.setChecked(false);
                        }
                        if (obj.isLunchAvailable()) {
                            lunchCheck.setChecked(true);
                        } else {
                            lunchCheck.setChecked(false);
                        }
                        if (obj.isDinnerAvailable()) {
                            dinnerCheck.setChecked(true);
                        } else {
                            dinnerCheck.setChecked(false);
                        }
                    }
                } else if (Util.getPrefs(getActivity()).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == CUSTOMER_SELECTION) {
                    cuEateryName.setText(obj.getEateryName());
                    cuEateryYear.setText(obj.getYearOfEstablishment());
                    cuEateryAddress.setText(obj.getAddress());
                    cuEateryMobile.setText(obj.getMobileNo());
                    if (obj.isBreakfastAvailable()) {
                        bfCheck.setChecked(true);
                    } else {
                        bfCheck.setChecked(false);
                    }
                    if (obj.isLunchAvailable()) {
                        lunchCheck.setChecked(true);
                    } else {
                        lunchCheck.setChecked(false);
                    }
                    if (obj.isDinnerAvailable()) {
                        dinnerCheck.setChecked(true);
                    } else {
                        dinnerCheck.setChecked(false);
                    }
                    if ("".equals(Util.getPrefs(getContext()).getString(EATERY_LOGO, ""))) {
                        Picasso.with(getContext()).load(R.drawable.add_photo)
                                .fit()
                                .into(eateryLogo);
                    } else {
                        Picasso.with(getContext()).load(Util.getPrefs(getContext()).getString(EATERY_LOGO, ""))
                                .placeholder(ContextCompat.getDrawable(getContext(), R.drawable.add_photo))
                                .fit()
                                .into(eateryLogo);
                    }
                }
            }
        } catch (JsonSyntaxException ex) {

        }
    }

    private void registerForEventBus() {

        try {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);

        } catch (ClassCastException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    private void processNewLocations(String lat, String lng) {
        Log.v("Test", "Inside Function processNewLocations");
        mLastLocation = new Location("");
        mLastLocation.setLongitude(Double.parseDouble(lng));
        mLastLocation.setLatitude(Double.parseDouble(lat));
        GetLocationBackgroundTask obj = new GetLocationBackgroundTask();
        obj.execute();
        Gson gson = new Gson();
        String json = Util.getPrefs(getParentFragment().getActivity()).getString(EATERY_OBJECT, "");
        EateryObject eateryObject = gson.fromJson(json, EateryObject.class);
        if (eateryObject == null) {
            eateryObject = new EateryObject();
        }
        eateryObject.setLatitude(Double.parseDouble(lat));
        eateryObject.setLongitude(Double.parseDouble(lng));
        json = gson.toJson(eateryObject);
        Util.getPrefs(getParentFragment().getActivity()).edit().putString(EATERY_OBJECT, json).apply();
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            // key = "key=AIzaSyBxB6KbGdfhQpOnFFVpq8AgWBro2Gdy4e4";
            key = "key=" + SERVER_KEY;

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters;
            parameters = input + "&" + sensor + "&" + key + "&radius=752360";
            // String parameters =
            // input+"&"+types+"&"+sensor+"&"+key+"&components=country:in";

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;
            Log.e("URL", url);
            try {
                // Fetching the data from web service in background
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Starting Parsing the JSON string returned by Web Service
            new ParserTask().execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        private JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(final List<HashMap<String, String>> result) {

            // Creating a SimpleAdapter for the AutoCompleteTextView
            // SimpleAdapter adapter = new SimpleAdapter(getBaseContext(),
            // result, android.R.layout.simple_list_item_1, from, to);
            ArrayList<String> lst = new ArrayList<String>();

            if (result == null)
                return;

            for (HashMap<String, String> h : result) {
                lst.add(h.get("description"));

            }
            // String str=lst.toString();
            // atvPlaces.setText(str);
            FilterWithSpaceAdapter<String> adapter = new FilterWithSpaceAdapter<String>(getContext(),
                    R.layout.item_autocomplete, android.R.id.text1, lst);
            // Setting the adapter
            owEateryAddress.setAdapter(adapter);
            owEateryAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                    // String selection =
                    // (String)parent.getItemAtPosition(position);
                    Log.v("Test", "Inside Function onItemClick Position -- "+position + " -- " +result);
                    String placeid = result.get(position).get("place_id");
                    new GetLatLong(placeid).execute();
                }
            });
        }
    }

    private class GetLatLong extends AsyncTask<Void, Void, String> {
        private String placeid;

        public GetLatLong(String place) {
            placeid = place;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.v("Test", "Inside Function onPostExecute of GetLatLong Result " + result);
            try {
                JSONObject jobj = new JSONObject(result);
                JSONObject jobj1 = jobj.getJSONObject("result");
                JSONObject jobj2 = jobj1.getJSONObject("geometry").getJSONObject("location");
                String lat = jobj2.getString("lat");
                String lng = jobj2.getString("lng");
                // latStr = lat;
                // lngStr = lng;

                processNewLocations(lat, lng);

                Log.v("Test", "Outside Function onPostExecute of GetLatLong");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            String data = null;
            String url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeid + "&" + key;
            Log.e("URL", url);
            try {
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Eaten", "Exception while downloading url " + e.toString());
        } finally {
            if (iStream != null) {
                iStream.close();
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return data;
    }

    private class GetLocationBackgroundTask extends AsyncTask<String, String, Address> {
        private Geocoder geocoder;
        private Address address;

        public GetLocationBackgroundTask() {
        }

        @Override
        protected Address doInBackground(String... params) {
            // TODO Auto-generated method stub
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
                if (addresses != null && addresses.size() > 0) {
                    address = addresses.get(0);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return address;
        }

        @Override
        protected void onPostExecute(Address result) {
            super.onPostExecute(result);
            Log.v("Test", "Event Eatery Location Selected");
            EventBusListener eventBusListener = new EventBusListener();
            eventBusListener.setType(AppConstant.EVENT_EATERY_LOC_SELECTED);
            eventBusListener.setData(address);
            EventBus.getDefault().post(eventBusListener);
        }
    }
}
