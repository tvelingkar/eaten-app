package com.app.eaten.eatery;

/**
 * Created by hl0395 on 28/9/15.
 */
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Created by albert on 14-3-21.
 */
public class MultipartReq extends Request<String> {
    public static final String KEY_PICTURE = "mypicture";
    public static final String KEY_PICTURE_NAME = "filename";
    public static final String KEY_ROUTE_ID = "route_id";

    private HttpEntity mHttpEntity;

    private String mRouteId;
    private Response.Listener mListener;

    public MultipartReq(String url, File filePath, String routeId,
                            Response.Listener<String> listener,
                            Response.ErrorListener errorListener,
                            String iName,String iPrice,
                            String uniqueID, String eateryID) {
        super(Method.POST, url, errorListener);

        mRouteId = routeId;
        mListener = listener;
        mHttpEntity = buildMultipartEntity(filePath,iName,iPrice,uniqueID,eateryID);
    }

    public MultipartReq(String url, File file, String routeId,
                            Response.Listener<String> listener,
                            Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);

        mRouteId = routeId;
        mListener = listener;
//        mHttpEntity = buildMultipartEntity(file);
    }

//    private HttpEntity buildMultipartEntity(String filePath) {
//        File file = new File(filePath);
//        return buildMultipartEntity(file);
//    }

    private HttpEntity buildMultipartEntity(File file,String iName,String iPrice,String uniqueID,String eateryID) {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        String fileName = file.getName();
        FileBody fileBody = new FileBody(file);
        builder.addPart("upload", fileBody);
        builder.addTextBody("dishName",iName);
        builder.addTextBody("dishPrice", iPrice);
        builder.addTextBody("uniqueId",uniqueID);
        builder.addTextBody("eateryId",eateryID);
        return builder.build();
    }

    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        Log.v("Eaten", "Response -- "+response.data);
        return Response.success("Uploaded", getCacheEntry());
    }

    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }
}
