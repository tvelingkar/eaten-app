package com.app.eaten.eatery;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.eaten.CommonActivity;
import com.app.eaten.R;
import com.app.eaten.util.EatenConstants;
import com.app.eaten.util.Util;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class EateryInfoFragment extends Fragment implements EatenConstants{

    View eateryInfoView;

    TextView eateryName,eateryLocation;

    public EateryInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        eateryInfoView=inflater.inflate(R.layout.fragment_eatery_info, container, false);
        setupViews();
        return eateryInfoView;
    }

    private void setupViews(){
        eateryName=(TextView)eateryInfoView.findViewById(R.id.eateryNameTxt);
        eateryLocation=(TextView)eateryInfoView.findViewById(R.id.eateryLocationTxt);
        sendMyEateryRequest();
    }

    private void sendMyEateryRequest(){
        JsonObjectRequest myEateryRequest = new JsonObjectRequest(Request.Method.GET, SERVER_BASE_URL + MYEATERY_URL+
                "?uniqueId="+ Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID,""),
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                Toast.makeText(getActivity(), "Successfully logged in!!!", Toast.LENGTH_SHORT).show();
                Log.d("TAG", "response --- " + response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getActivity(), "Error while retrieving my eatery!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };

        ((CommonActivity)getActivity()).addRequestToQueue(myEateryRequest);

    }


}
