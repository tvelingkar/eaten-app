package com.app.eaten.eatery;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.eaten.CommonActivity;
import com.app.eaten.R;
import com.app.eaten.beans.EateryObject;
import com.app.eaten.beans.EateryTime;
import com.app.eaten.constants.AppConstant;
import com.app.eaten.eventbus.EventBusListener;
import com.app.eaten.util.CuisineTypeOptions;
import com.app.eaten.util.EatenConstants;
import com.app.eaten.util.EateryTimeList;
import com.app.eaten.util.EateryTypeOptions;
import com.app.eaten.util.MultiSpinner;
import com.app.eaten.util.Util;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class EateryTypeFragment extends Fragment implements EatenConstants {

    View eateryTypeView;
    EditText costForTwo;
    SwitchCompat deliveryToggle, cateringAvailableToggle;
    Button monBFBtn, monLunchBtn, monDinnerBtn, tueBFBtn, tueLunchBtn, tueDinnerBtn, wedBFBtn, wedLunchBtn, wedDinnerBtn,
            thuBFBtn, thuLunchBtn, thuDinnerBtn, friBFBtn, friLunchBtn, friDinnerBtn, satBFBtn, satLunchBtn, satDinnerBtn,
            sunBFBtn, sunLunchBtn, sunDinnerBtn, submitBtn;
    EditText chefNameEdit, aboutUsEdit;
    private ArrayAdapter<String> paymentTypeAdapter;
    private StringBuilder selectedEaterytype = new StringBuilder("");
    private StringBuilder selectedCuisinetype = new StringBuilder("");
    private StringBuilder selectedEateryTypeValue = new StringBuilder("");
    private StringBuilder selectedCuisineTypeValue = new StringBuilder("");
    private StringBuilder selectedPaymenttype = new StringBuilder("");
    private MultiSpinner spnEateryType;
    private MultiSpinner spnCuisineType;
    private MultiSpinner spnPaymentType;

    private final MultiSpinner.MultiSpinnerListener onEateryTypeSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    selectedEaterytype.append(EateryTypeOptions.getKey(i)).append(",");
                    selectedEateryTypeValue.append(EateryTypeOptions.getEntry(i)).append(",");
                }
            }
            if (selectedEaterytype.length() > 0) {
                selectedEaterytype.deleteCharAt(selectedEaterytype.length() - 1);
                selectedEateryTypeValue.deleteCharAt(selectedEateryTypeValue.length() - 1);
            }
        }
    };

    private final MultiSpinner.MultiSpinnerListener onCuisineTypeSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    selectedCuisinetype.append(CuisineTypeOptions.getKey(i)).append(",");
                    selectedCuisineTypeValue.append(CuisineTypeOptions.getEntry(i)).append(",");
                }
            }
            if (selectedCuisinetype.length() > 0) {
                selectedCuisinetype.deleteCharAt(selectedCuisinetype.length() - 1);
                selectedCuisineTypeValue.deleteCharAt(selectedCuisineTypeValue.length() - 1);
            }
        }
    };

    private final MultiSpinner.MultiSpinnerListener onPaymentTypeSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    selectedPaymenttype.append(paymentTypeAdapter.getItem(i)).append(",");
                }
            }
            if (selectedPaymenttype.length() >0) {
                selectedPaymenttype.deleteCharAt(selectedPaymenttype.length() - 1);
            }
        }
    };

    public EateryTypeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        eateryTypeView = inflater.inflate(R.layout.fragment_eatery_type, container, false);
        setupViews();
        registerForEventBus();
        setupUIfromLocal();
        return eateryTypeView;
    }

    private void setupViews() {
        spnEateryType = (MultiSpinner) eateryTypeView.findViewById(R.id.spnEateryType);
        setSpinner(spnEateryType, EateryTypeOptions.getAllData(), EATERY_TYPE);
        spnCuisineType = (MultiSpinner) eateryTypeView.findViewById(R.id.spnCuisineType);
        setSpinner(spnCuisineType, CuisineTypeOptions.getAllData(), CUISINE_TYPE);
        spnPaymentType = (MultiSpinner) eateryTypeView.findViewById(R.id.spnPaymentType);
        setSpinner(spnPaymentType, AppConstant.getPaymentOptions(), PAYMENT_TYPE);
        deliveryToggle = (SwitchCompat) eateryTypeView.findViewById(R.id.deliveryToggle);
        cateringAvailableToggle = (SwitchCompat) eateryTypeView.findViewById(R.id.cateringAvailabilityToggle);
        chefNameEdit = (EditText) eateryTypeView.findViewById(R.id.chefNameEdit);
        aboutUsEdit = (EditText) eateryTypeView.findViewById(R.id.aboutEdit);

        submitBtn = (Button) eateryTypeView.findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendAddEateryRequest();
            }
        });

        monBFBtn = (Button) eateryTypeView.findViewById(R.id.mondayBFBtn);
        monLunchBtn = (Button) eateryTypeView.findViewById(R.id.mondayLunchBtn);
        monDinnerBtn = (Button) eateryTypeView.findViewById(R.id.mondayDinnerBtn);
        tueBFBtn = (Button) eateryTypeView.findViewById(R.id.tuesdayBFBtn);
        tueLunchBtn = (Button) eateryTypeView.findViewById(R.id.tuesdayLunchBtn);
        tueDinnerBtn = (Button) eateryTypeView.findViewById(R.id.tuesdayDinnerBtn);
        wedBFBtn = (Button) eateryTypeView.findViewById(R.id.wedBFBtn);
        wedLunchBtn = (Button) eateryTypeView.findViewById(R.id.wedLunchBtn);
        wedDinnerBtn = (Button) eateryTypeView.findViewById(R.id.wedDinnerBtn);
        thuBFBtn = (Button) eateryTypeView.findViewById(R.id.thursdayBFBtn);
        thuLunchBtn = (Button) eateryTypeView.findViewById(R.id.thursdayLunchBtn);
        thuDinnerBtn = (Button) eateryTypeView.findViewById(R.id.thursdayDinnerBtn);
        friBFBtn = (Button) eateryTypeView.findViewById(R.id.fridayBFBtn);
        friLunchBtn = (Button) eateryTypeView.findViewById(R.id.fridayLunchBtn);
        friDinnerBtn = (Button) eateryTypeView.findViewById(R.id.fridayDinnerBtn);
        satBFBtn = (Button) eateryTypeView.findViewById(R.id.saturdayBFBtn);
        satLunchBtn = (Button) eateryTypeView.findViewById(R.id.saturdayLunchBtn);
        satDinnerBtn = (Button) eateryTypeView.findViewById(R.id.saturdayDinnerBtn);
        sunBFBtn = (Button) eateryTypeView.findViewById(R.id.sundayBFBtn);
        sunLunchBtn = (Button) eateryTypeView.findViewById(R.id.sundayLunchBtn);
        sunDinnerBtn = (Button) eateryTypeView.findViewById(R.id.sundayDinnerBtn);
        costForTwo = (EditText) eateryTypeView.findViewById(R.id.costForTwo);
        TextView costCurrency = (TextView) eateryTypeView.findViewById(R.id.costForTwoText);
        costCurrency.setText(String.format(costCurrency.getText().toString(), Util.getPrefs(getActivity()).getString(COUNTRY_CURRENCY, "Your Currency")));
        monBFBtn.setOnClickListener(timingClick);
        monLunchBtn.setOnClickListener(timingClick);
        monDinnerBtn.setOnClickListener(timingClick);
        tueBFBtn.setOnClickListener(timingClick);
        tueLunchBtn.setOnClickListener(timingClick);
        tueDinnerBtn.setOnClickListener(timingClick);
        wedBFBtn.setOnClickListener(timingClick);
        wedLunchBtn.setOnClickListener(timingClick);
        wedDinnerBtn.setOnClickListener(timingClick);
        thuBFBtn.setOnClickListener(timingClick);
        thuLunchBtn.setOnClickListener(timingClick);
        thuDinnerBtn.setOnClickListener(timingClick);
        friBFBtn.setOnClickListener(timingClick);
        friLunchBtn.setOnClickListener(timingClick);
        friDinnerBtn.setOnClickListener(timingClick);
        satBFBtn.setOnClickListener(timingClick);
        satLunchBtn.setOnClickListener(timingClick);
        satDinnerBtn.setOnClickListener(timingClick);
        sunBFBtn.setOnClickListener(timingClick);
        sunLunchBtn.setOnClickListener(timingClick);
        sunDinnerBtn.setOnClickListener(timingClick);

    }

    public void onEvent(EventBusListener mEventListener) {
        switch (mEventListener.getType()) {
            case AppConstant.EVENT_EATERY_DATA_FETCHED:
                setupUIfromLocal();
                break;
        }
    }

    private void setupUIfromLocal() {

        Gson gson = new Gson();
        String json = Util.getPrefs(getParentFragment().getActivity()).getString(EATERY_OBJECT, "");
        EateryObject eateryObject = gson.fromJson(json, EateryObject.class);
        if (eateryObject == null) {
            eateryObject = new EateryObject();
        }
        spnEateryType.setText(eateryObject.getEateryType());
        spnCuisineType.setText(eateryObject.getCuisineType());
        spnPaymentType.setText(eateryObject.getPaymentType());

        if (eateryObject.isDeliveryAvailable()) {
            deliveryToggle.setChecked(true);
        } else {
            deliveryToggle.setChecked(false);
        }
        if (eateryObject.isCateringAvailable()) {
            cateringAvailableToggle.setChecked(true);
        } else {
            cateringAvailableToggle.setChecked(false);
        }
        chefNameEdit.setText(eateryObject.getChefName());
        aboutUsEdit.setText(eateryObject.getAboutEatery());
        costForTwo.setText(String.valueOf(eateryObject.getCostForTwo()));

        int size = EateryTimeList.fetchAllEntries().size();
        int i = 0;
        EateryTime eateryTime;
        for (; i < size; i++) {
            eateryTime = EateryTimeList.fetchAllEntries().valueAt(i);
            if (i == 0) {
                monBFBtn.setText(eateryTime.breakfast);
                monLunchBtn.setText(eateryTime.lunch);
                monDinnerBtn.setText(eateryTime.dinner);
            } else if (i == 1) {
                tueBFBtn.setText(eateryTime.breakfast);
                tueLunchBtn.setText(eateryTime.lunch);
                tueDinnerBtn.setText(eateryTime.dinner);
            } else if (i == 2) {
                wedBFBtn.setText(eateryTime.breakfast);
                wedLunchBtn.setText(eateryTime.lunch);
                wedDinnerBtn.setText(eateryTime.dinner);
            } else if (i == 3) {
                thuBFBtn.setText(eateryTime.breakfast);
                thuLunchBtn.setText(eateryTime.lunch);
                thuDinnerBtn.setText(eateryTime.dinner);
            } else if (i == 4) {
                friBFBtn.setText(eateryTime.breakfast);
                friLunchBtn.setText(eateryTime.lunch);
                friDinnerBtn.setText(eateryTime.dinner);
            } else if (i == 5) {
                satBFBtn.setText(eateryTime.breakfast);
                satLunchBtn.setText(eateryTime.lunch);
                satDinnerBtn.setText(eateryTime.dinner);
            } else if (i == 6) {
                sunBFBtn.setText(eateryTime.breakfast);
                sunLunchBtn.setText(eateryTime.lunch);
                sunDinnerBtn.setText(eateryTime.dinner);
            }
        }
    }

    private void sendAddEateryRequest() {

        JSONObject saveEateryObj = new JSONObject();

        JSONObject request = new JSONObject();

        Gson gson = new Gson();
        String json = Util.getPrefs(getParentFragment().getActivity()).getString(EATERY_OBJECT, "");
        EateryObject eateryObject = gson.fromJson(json, EateryObject.class);
        try {

            if (eateryObject == null) {
                eateryObject = new EateryObject();
            }
            eateryObject.setEateryType(selectedEateryTypeValue.toString());
            eateryObject.setCuisineType(selectedCuisineTypeValue.toString());
            eateryObject.setPaymentType(selectedPaymenttype.toString());
            eateryObject.setChefName(chefNameEdit.getText().toString());
            eateryObject.setCostForTwo(Integer.parseInt(costForTwo.getText().toString()));
            eateryObject.setAboutEatery(aboutUsEdit.getText().toString());
            request.put("eateryType", selectedEaterytype.toString());
            request.put("cuisineType", selectedCuisinetype.toString());
            request.put("eateryName", eateryObject.getEateryName());
            request.put("establishedYear", eateryObject.getYearOfEstablishment());
            request.put("firstAddress", eateryObject.getAddress());
            request.put("latitude", eateryObject.getLatitude());
            request.put("longitude", eateryObject.getLongitude());
            request.put("area", eateryObject.getArea());
            request.put("city", eateryObject.getCity());
            request.put("state", eateryObject.getState());
            request.put("country", eateryObject.getCountry());
            request.put("pincode", eateryObject.getPincode());
            request.put("locationCode", eateryObject.getPincode());
            request.put("mobile", eateryObject.getMobileNo());
            request.put("breakfast", "N");
            request.put("lunch", "N");
            request.put("dinner", "N");
            if (eateryObject.isBreakfastAvailable()) {
                request.put("breakfast", "Y");
            }
            if (eateryObject.isLunchAvailable()) {
                request.put("lunch", "Y");
            }
            if (eateryObject.isDinnerAvailable()) {
                request.put("dinner", "Y");
            }
            request.put("VegNonVeg", "");
            if (deliveryToggle.isChecked()) {
                eateryObject.setIsDeliveryAvailable(true);
                request.put("delivery", "Y");
            } else {
                request.put("delivery", "N");
            }
            if (cateringAvailableToggle.isChecked()) {
                eateryObject.setIsCateringAvailable(true);
                request.put("cateringAvailable", "Y");
            } else {
                request.put("cateringAvailable", "N");
            }
            request.put("payment", selectedPaymenttype.toString());
            request.put("chefName", chefNameEdit.getText().toString());
            request.put("hireChef", "N");
            request.put("costForTwo", costForTwo.getText().toString());
            request.put("about", aboutUsEdit.getText().toString());

            JSONArray timimgArray = new JSONArray();

            EateryTime eateryTime = new EateryTime();
            eateryTime.breakfast = monBFBtn.getText().toString();
            eateryTime.lunch = monLunchBtn.getText().toString();
            eateryTime.dinner = monDinnerBtn.getText().toString();
            EateryTimeList.addEntry(0, eateryTime);
            JSONObject mondayJson = new JSONObject();
            mondayJson.put("day", "monday");
            mondayJson.put("breakfast", monBFBtn.getText().toString());
            mondayJson.put("lunch", monLunchBtn.getText().toString());
            mondayJson.put("dinner", monDinnerBtn.getText().toString());
            timimgArray.put(mondayJson);

            eateryTime = new EateryTime();
            eateryTime.breakfast = tueBFBtn.getText().toString();
            eateryTime.lunch = tueLunchBtn.getText().toString();
            eateryTime.dinner = tueDinnerBtn.getText().toString();
            EateryTimeList.addEntry(1, eateryTime);
            JSONObject tuesdayJson = new JSONObject();
            tuesdayJson.put("day", "tuesday");
            tuesdayJson.put("breakfast", tueBFBtn.getText().toString());
            tuesdayJson.put("lunch", tueLunchBtn.getText().toString());
            tuesdayJson.put("dinner", tueDinnerBtn.getText().toString());
            timimgArray.put(tuesdayJson);

            eateryTime = new EateryTime();
            eateryTime.breakfast = wedBFBtn.getText().toString();
            eateryTime.lunch = wedLunchBtn.getText().toString();
            eateryTime.dinner = wedDinnerBtn.getText().toString();
            EateryTimeList.addEntry(2, eateryTime);
            JSONObject wedJson = new JSONObject();
            wedJson.put("day", "wednesday");
            wedJson.put("breakfast", wedBFBtn.getText().toString());
            wedJson.put("lunch", wedLunchBtn.getText().toString());
            wedJson.put("dinner", wedDinnerBtn.getText().toString());
            timimgArray.put(wedJson);

            eateryTime = new EateryTime();
            eateryTime.breakfast = thuBFBtn.getText().toString();
            eateryTime.lunch = thuLunchBtn.getText().toString();
            eateryTime.dinner = thuDinnerBtn.getText().toString();
            EateryTimeList.addEntry(3, eateryTime);
            JSONObject thuJson = new JSONObject();
            thuJson.put("day", "thursday");
            thuJson.put("breakfast", thuBFBtn.getText().toString());
            thuJson.put("lunch", thuLunchBtn.getText().toString());
            thuJson.put("dinner", thuDinnerBtn.getText().toString());
            timimgArray.put(thuJson);

            eateryTime = new EateryTime();
            eateryTime.breakfast = friBFBtn.getText().toString();
            eateryTime.lunch = friLunchBtn.getText().toString();
            eateryTime.dinner = friDinnerBtn.getText().toString();
            EateryTimeList.addEntry(4, eateryTime);
            JSONObject friJson = new JSONObject();
            friJson.put("day", "friday");
            friJson.put("breakfast", friBFBtn.getText().toString());
            friJson.put("lunch", friLunchBtn.getText().toString());
            friJson.put("dinner", friDinnerBtn.getText().toString());
            timimgArray.put(friJson);

            eateryTime = new EateryTime();
            eateryTime.breakfast = satBFBtn.getText().toString();
            eateryTime.lunch = satLunchBtn.getText().toString();
            eateryTime.dinner = satDinnerBtn.getText().toString();
            EateryTimeList.addEntry(5, eateryTime);
            JSONObject satJson = new JSONObject();
            satJson.put("day", "saturday");
            satJson.put("breakfast", satBFBtn.getText().toString());
            satJson.put("lunch", satLunchBtn.getText().toString());
            satJson.put("dinner", satDinnerBtn.getText().toString());
            timimgArray.put(satJson);

            eateryTime = new EateryTime();
            eateryTime.breakfast = sunBFBtn.getText().toString();
            eateryTime.lunch = sunLunchBtn.getText().toString();
            eateryTime.dinner = sunDinnerBtn.getText().toString();
            EateryTimeList.addEntry(6, eateryTime);
            JSONObject sunJson = new JSONObject();
            sunJson.put("day", "sunday");
            sunJson.put("breakfast", sunBFBtn.getText().toString());
            sunJson.put("lunch", sunLunchBtn.getText().toString());
            sunJson.put("dinner", sunDinnerBtn.getText().toString());
            timimgArray.put(sunJson);

            request.put("timing", timimgArray);

            request.put("uniqueId", Util.getPrefs(getActivity()).getString(LOGIN_UNIQUE_ID, ""));
            saveEateryObj.putOpt("data", request);

            json = gson.toJson(eateryObject);
            Util.getPrefs(getContext()).edit().putString(EATERY_OBJECT, json).apply();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("TAG", "request --- " + saveEateryObj.toString());

        if (eateryObject.getEateryName() == null || eateryObject.getEateryName().isEmpty() || eateryObject.getYearOfEstablishment() == null || eateryObject.getYearOfEstablishment().isEmpty() || eateryObject.getAddress() == null || eateryObject.getAddress().isEmpty()) {
            EventBusListener eventbusListener = new EventBusListener();
            eventbusListener.setType("NAVIGATE_BACK_DATA_INCOMPLETE");
            EventBus.getDefault().post(eventbusListener);
        } else {

            JsonObjectRequest saveEateryRequest = new JsonObjectRequest(Request.Method.POST, SERVER_BASE_URL + SAVE_EATERY_URL, saveEateryObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("TAG", "response --- " + response.toString());
                    try {
                        if (OPTION_NO.equals(Util.getPrefs(getContext()).getString(IS_EATERY_CREATED, OPTION_NO))) {
                            Util.getPrefs(getContext()).edit().putString(IS_EATERY_CREATED, OPTION_YES).apply();
                            Util.getPrefs(getContext()).edit().putString(EATERY_ID, response.getString("eateryID")).apply();
                            Toast.makeText(getParentFragment().getActivity(), "Eatery created Successfully !!!", Toast.LENGTH_LONG).show();

                            EventBusListener eventbusListener = new EventBusListener();
                            eventbusListener.setType("ADD_GALLERY_FRAGMENT");
                            EventBus.getDefault().post(eventbusListener);
                        } else {
                            Util.getPrefs(getActivity()).edit().putBoolean(IS_TEMP_USER, true).apply();
                            Toast.makeText(getParentFragment().getActivity(), "Eatery Data updated Successfully !!!", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(), "Error while saving the eatery!!!", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
            };

            ((CommonActivity) getActivity()).addRequestToQueue(saveEateryRequest);
        }

    }

    private View.OnClickListener timingClick = new View.OnClickListener() {
        @Override
        public void onClick(final View v1) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();

            View dialogView = inflater.inflate(R.layout.eatery_timings_layout, null);
            dialogBuilder.setView(dialogView);

            final Spinner startSpinner = (Spinner) dialogView.findViewById(R.id.startTimeSpinner);
            ArrayAdapter<String> startSpinnerAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.time_array));
            startSpinner.setAdapter(startSpinnerAdapter);

            final Spinner endSpinner = (Spinner) dialogView.findViewById(R.id.endTimeSpinner);
            ArrayAdapter<String> endSpinnerAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.time_array));
            endSpinner.setAdapter(endSpinnerAdapter);

            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();

            Button setTimeBtn = (Button) dialogView.findViewById(R.id.setTimeBtn);
            setTimeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((Button) v1 == monBFBtn) {

                        monBFBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        tueBFBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        wedBFBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        thuBFBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        friBFBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        satBFBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        sunBFBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                    } else if ((Button) v1 == monLunchBtn) {

                        monLunchBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        tueLunchBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        wedLunchBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        thuLunchBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        friLunchBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        satLunchBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        sunLunchBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                    } else if ((Button) v1 == monDinnerBtn) {

                        monDinnerBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        tueDinnerBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        wedDinnerBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        thuDinnerBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        friDinnerBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        satDinnerBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                        sunDinnerBtn.setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());

                    } else {
                        ((Button) v1).setText(startSpinner.getSelectedItem().toString() + " - " + endSpinner.getSelectedItem().toString());
                    }
                    alertDialog.dismiss();
                }
            });

        }
    };

    private void registerForEventBus() {

        try {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);

        } catch (ClassCastException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    private void setSpinner(MultiSpinner multiSpinner, ArrayList<String> typeData, String spnType) {
        if (EATERY_TYPE.equals(spnType)) {
            ArrayAdapter<String> eateryTypeAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, typeData);
            multiSpinner.setAdapter(eateryTypeAdapter, false, onEateryTypeSelectedListener);
            multiSpinner.setText("Select Eatery Type");
        } else if (CUISINE_TYPE.equals(spnType)) {
            ArrayAdapter<String> cuisineTypeAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, typeData);
            multiSpinner.setAdapter(cuisineTypeAdapter, false, onCuisineTypeSelectedListener);
            multiSpinner.setText("Select Cuisine Type");
        } else if (PAYMENT_TYPE.equals(spnType)) {
            paymentTypeAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, typeData);
            multiSpinner.setAdapter(paymentTypeAdapter, false, onPaymentTypeSelectedListener);
            multiSpinner.setText("Select Payment Type");
        }
    }

}
