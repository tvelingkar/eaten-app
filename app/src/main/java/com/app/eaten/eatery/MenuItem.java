package com.app.eaten.eatery;

/**
 * Created by hl0395 on 15/8/15.
 */
public class MenuItem {

    public String menuImgURL;
    public String menuName;
    public String menuPrice;
    public String menuID;


    public String getMenuID() {
        return menuID;
    }

    public void setMenuID(String menuID) {
        this.menuID = menuID;
    }

    public String getMenuImgURL() {
        return menuImgURL;
    }

    public void setMenuImgURL(String menuImgURL) {
        this.menuImgURL = menuImgURL;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(String menuPrice) {
        this.menuPrice = menuPrice;
    }
}
