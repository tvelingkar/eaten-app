package com.app.eaten.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.eaten.CommonActivity;
import com.app.eaten.R;
import com.app.eaten.beans.CommonTypeObject;
import com.app.eaten.home.HomeActivity;
import com.app.eaten.util.CuisineTypeOptions;
import com.app.eaten.util.EatenConstants;
import com.app.eaten.util.EateryTypeOptions;
import com.app.eaten.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

public class SplashActivity extends CommonActivity implements EatenConstants {

    private static final int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        if (Util.getPrefs(SplashActivity.this).getBoolean(LOGIN_STATUS, false)) {
            if (Util.getPrefs(this).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
                if (EateryTypeOptions.getCount() == 0 || CuisineTypeOptions.getCount() == 0) {
                    fetchEateryOptions();
                }
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void fetchEateryOptions() {

        JsonObjectRequest updateMenuRequest = new JsonObjectRequest(Request.Method.GET, SERVER_BASE_URL +
                FETCH_EATERY_CUISINE_TYPE + "?uniqueId=" + Util.getPrefs(this).getString(LOGIN_UNIQUE_ID, ""), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                JSONArray objectType = response.optJSONArray("EateryType");
                int count;
                int i;
                JSONObject object;
                if (objectType != null) {
                    count = objectType.length();
                    CommonTypeObject typeObject;
                    for (i = 0; i < count; i++) {
                        object = objectType.optJSONObject(i);
                        if (object != null) {
                            typeObject = new CommonTypeObject();
                            typeObject.id = object.optInt("id");
                            typeObject.name = object.optString("name");
                            EateryTypeOptions.addEntry(typeObject);
                        }
                    }
                }

                objectType = response.optJSONArray("CuisineType");
                if (objectType != null) {
                    count = objectType.length();
                    CommonTypeObject typeObject;
                    for (i = 0; i < count; i++) {
                        object = objectType.optJSONObject(i);
                        if (object != null) {
                            typeObject = new CommonTypeObject();
                            typeObject.id = object.optInt("id");
                            typeObject.name = object.optString("name");
                            CuisineTypeOptions.addEntry(typeObject);
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        addRequestToQueue(updateMenuRequest);
    }
}
