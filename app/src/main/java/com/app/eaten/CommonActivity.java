package com.app.eaten;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.Volley;
import com.app.eaten.util.EatenConstants;

public class CommonActivity extends AppCompatActivity implements EatenConstants {

    FragmentTransaction fragmentTransaction;
    public FragmentManager fragmentManager;

    public RequestQueue queue;
    int requestTimeout = 60000;
    RetryPolicy policy;
    public Request currentStringRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        queue = Volley.newRequestQueue(CommonActivity.this);

        if (fragmentManager == null)
            fragmentManager = getSupportFragmentManager();
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void addRequestToQueue(Request request) {
        if(queue==null){
            queue = Volley.newRequestQueue(CommonActivity.this);
        }
        policy = new DefaultRetryPolicy(requestTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        currentStringRequest = request;
        Log.d("TUSSI", currentStringRequest.getUrl());
        queue.add(request);
    }


    public void addFragment(Fragment fragment, boolean addStack,
                            boolean isReplace) {
        try {
            if (fragment == null) {
                return;
            }

            if (fragmentManager == null) {
                fragmentManager = getSupportFragmentManager();
            }

            fragmentTransaction = fragmentManager.beginTransaction();
            String tag = fragment.getClass().toString();

            if (fragment.isAdded()) {
                fragmentTransaction.show(fragment);
            } else {
                if (!isReplace) {
                    fragmentTransaction.add(R.id.frame_layout, fragment, tag);
                } else {

                    fragmentTransaction.replace(R.id.frame_layout, fragment, tag);
                }
            }

            if (addStack) {
                fragmentTransaction.addToBackStack(tag);
            } else {
                fragmentManager.popBackStack(tag,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }

            fragmentTransaction.commit();
            fragmentManager.executePendingTransactions();
        } catch (Exception ex){
            ex.printStackTrace();
        }

    }
}
