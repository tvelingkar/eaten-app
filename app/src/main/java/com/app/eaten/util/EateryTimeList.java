package com.app.eaten.util;

import android.util.SparseArray;

import com.app.eaten.beans.EateryTime;

/**
 * Created by Tushar Velingkar on 4/12/15.
 */
public class EateryTimeList {
    private static final SparseArray<EateryTime> eateryTime = new SparseArray<>();

    public static void reset() {
        eateryTime.clear();
    }

    public static void addEntry(int key, EateryTime time) {
        eateryTime.put(key, time);
    }

    public static SparseArray<EateryTime> fetchAllEntries() {
        return eateryTime;
    }
}
