package com.app.eaten.util;

/**
 * Created by goku on 13/12/15.
 */

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import com.app.eaten.beans.EateryDish;
import com.app.eaten.beans.EateryObject;
import com.app.eaten.beans.ListEateryDishes;
import com.app.eaten.eventbus.EventBusListener;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import de.greenrobot.event.EventBus;

/**
 * Uploading the file to server
 * */
public class UploadFileToServer extends AsyncTask<String, Integer, String> implements EatenConstants {

    private ProgressDialog progressBar;
    private File sourceFile;
    private long totalSize = 0;
    private Context context;
    private String url;
    private int type;
    private String imageUri;
    private String dishName;
    private String dishPrice;

    public UploadFileToServer(File file, Context context, String url, int type) {
        sourceFile = file;
        this.context = context;
        this.url = url;
        this.type = type;
    }

    @Override
    protected void onPreExecute() {
        progressBar = new ProgressDialog(context);
        // setting progress bar to zero
        progressBar.setProgress(0);
        progressBar.show();
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {

        // updating progress bar value
        progressBar.setProgress(progress[0]);
    }

    @Override
    protected String doInBackground(String... params) {
        return uploadFile(params);
    }

    @SuppressWarnings("deprecation")
    private String uploadFile(String... params) {
        String responseString;

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(SERVER_BASE_URL + url + "?uniqueId=" + Util.getPrefs(context).getString(LOGIN_UNIQUE_ID, ""));
        Log.v("Eaten","Inside Upload "+SERVER_BASE_URL + url + "?uniqueId=" + Util.getPrefs(context).getString(LOGIN_UNIQUE_ID, ""));
        try {
            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE,
                    new AndroidMultiPartEntity.ProgressListener() {

                        @Override
                        public void transferred(long num) {
                            publishProgress((int) ((num / (float) totalSize) * 100));
                        }
                    });

            // Adding file data to http body
            FileBody fileBody = new FileBody(sourceFile);
            entity.addPart("upload", fileBody);

            // Extra parameters if you want to pass to server
            if (type == UPLOAD_EATERY_DISH || type == UPLOAD_EATERY_LOGO) {
                entity.addPart("eateryId", new StringBody(Util.getPrefs(context).getString(EATERY_ID, "")));
                Log.v("Eaten", "Eatery Id "+Util.getPrefs(context).getString(EATERY_ID, ""));
            }
            if (type == UPLOAD_EATERY_DISH) {
                dishName = params[0];
                dishPrice = params[1];
                imageUri = params[2];
                entity.addPart("dishName", new StringBody(dishName));
                entity.addPart("dishPrice", new StringBody(dishPrice));
            }

            totalSize = entity.getContentLength();
            httppost.setEntity(entity);

            // Making server call
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity r_entity = response.getEntity();

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // Server response
                responseString = EntityUtils.toString(r_entity);
            } else {
                responseString = "Error occurred! Http Status Code: "
                        + statusCode;
            }

        } catch (IOException e) {
            responseString = e.toString();
        }

        return responseString;

    }

    @Override
    protected void onPostExecute(String result) {
        Log.e("Eaten", "Response from server: " + result);
        progressBar.dismiss();

        if (type == UPLOAD_EATERY_DISH) {
            EateryDish eateryDish = new EateryDish();
            try {
                JSONObject dishObject = new JSONObject(result);
                eateryDish.imageId = dishObject.optInt("dishId");
                eateryDish.imageUrl = dishObject.optString("path");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            eateryDish.dishName = dishName;
            eateryDish.dishPrice = Integer.parseInt(dishPrice);
            eateryDish.imageLocalUri = imageUri;
            EateryDishesList.addEntry(eateryDish);

            String json = Util.getPrefs(context).getString(EATERY_DISH_LIST_OBJECT, "");
            Gson gson = new Gson();
            ListEateryDishes eateryDishes = gson.fromJson(json, ListEateryDishes.class);
            if (eateryDishes == null) {
                eateryDishes = new ListEateryDishes();
            }
            int key = eateryDishes.getEateryDishes().size();
            eateryDishes.getEateryDishes().add(key, eateryDish);
            json = gson.toJson(eateryDishes);
            Util.getPrefs(context).edit().putString(EATERY_DISH_LIST_OBJECT, json).apply();

            EventBusListener eventbusListener1 = new EventBusListener();
            eventbusListener1.setType("MENU_UPLOADED");
            EventBus.getDefault().post(eventbusListener1);
        } else if (type == UPLOAD_EATERY_LOGO) {
            EventBusListener eventbusListener1 = new EventBusListener();
            eventbusListener1.setType("LOGO_UPLOADED");
            EventBus.getDefault().post(eventbusListener1);
        }

        super.onPostExecute(result);
    }

}
