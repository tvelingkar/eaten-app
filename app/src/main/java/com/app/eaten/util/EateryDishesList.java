package com.app.eaten.util;

import com.app.eaten.beans.EateryDish;

import java.util.ArrayList;

/**
 * Created by Tushar Velingkar on 4/12/15.
 */
public class EateryDishesList {

    private static final ArrayList<EateryDish> eateryDishes = new ArrayList<>();

    public static void reset() {
        eateryDishes.clear();
    }

    public static void addEntry(EateryDish dish) {
        int key = eateryDishes.size();
        eateryDishes.add(key, dish);
    }

    public static ArrayList<EateryDish> fetchAllEntries() {
        return eateryDishes;
    }
}
