package com.app.eaten.util;

import android.os.Environment;

/**
 * Created by Vivek CA on 11-07-2015.
 */
public interface EatenConstants {

    int CUSTOMER_SELECTION = 1;
    int EATERY_SELECTION = 2;
    String SELECTION_VALUE = "selection_value";
    String LOGIN_UNIQUE_ID = "uniqueid";
    String LOGIN_STATUS = "loginStatus";
    String IS_FIRST_TIME = "isFirstTime";
    String EATERY_ID = "eateryId";
    String EATERY_NAME = "eateryName";
    String EATERY_YEAR = "eateryYear";
    String EATERY_ADDRESS = "eateryAddress";
    String EATERY_MOBILE = "eateryMobile";
    String EATERY_TYPE = "eateryType";
    String CUISINE_TYPE = "cuisineType";
    String PAYMENT_TYPE = "paymentType";
    String EATERY_LOGO = "eateryLogo";
    String EATERY_DELIVERY_AVAILABLE = "deliveryAvailable";
    String EATERY_PAY_MODE = "payMode";
    String EATERY_CATERING_AV = "cateringAvailable";
    String EATERY_CHEF_NAME = "chefName";
    String EATERY_BREAKFAST = "breakfast";
    String EATERY_LUNCH = "lunch";
    String EATERY_DINNER = "dinner";
    String EATERY_VEG_NON = "VegNonVeg";
    String EATERY_HIRE_CHEF = "hireChef";
    String EATERY_COST_TWO = "costForTwo";
    String EATERY_ABOUT_US = "aboutUs";
    String COUNTRY_NAME = "country";
    String COUNTRY_CODE = "countryCode";
    String COUNTRY_CURRENCY = "countryCurrency";

    int EXIT_REQUEST_CODE = 101;
    int RC_SIGN_IN = 102;
    int REG_REQ = 103;
    int SELECTION_REQ = 104;
    int LOGIN_REQ = 105;
    int EDIT_PROFILE_REQ = 106;

    String SERVER_BASE_URL = "http://109.74.196.254:81/";
    String SIGNUP_URL = "msignup";
    String SIGNIN_URL = "signin";
    String SIGNIN_FACEBOOK_URL = "flogin";
    String SIGNIN_GOOGLE_URL = "glogin";
    String UPDATE_USER_INFO_URL = "userInfoAPI/updateUserBasic";
    String SAVE_EATERY_URL = "saveEatery";
    String UPLOAD_EATERY_DISH_URL = "uploadEateryDishesImage";
    String UPDATE_EATERY_DISH_URL = "updateEateryDish";
    String DELETE_EATERY_IMAGE_URL = "deleteEateryImage";
    String DELETE_EATERY_DISH_URL = "deleteEateryDish";
    String UPLOAD_EATERY_LOGO_URL = "uploadEateryLogo";
    String MYEATERY_URL = "myEatery";
    String POST_USER_REVIEW = "review";
    String POST_OWNER_COMMENT = "addComment";
    String UPLOAD_USER_PIC_URL = "userInfoAPI/uploaduserImage";
    String GET_COUNTRY_NAME_URL = "http://ipinfo.io/";
    String GET_COUNTRY_CODE_URL = "https://restcountries.eu/rest/v1/alpha?codes=";
    String FETCH_EATERY_CUISINE_TYPE = "addEatery";
    String VERIFY_USER_URL = "mverify";

    //Customer
    String CITY_AREA_LIST_URL = "cityAreaList";
    String GET_EATERY_LIST_URL = "eatery";

    String TXT_NA = "N/A";

    String USER_EMAIL = "email";
    String USER_FNAME = "fname";
    String USER_LNAME = "lname";
    String USER_PROFILE_URI = "profileURI";
    String USER_DOB = "dob";
    String USER_AGE = "age";
    String USER_CITY = "city";
    String USER_MOBILE = "mobile";
    String USER_FOODIE = "foodie";
    String USER_GENDER = "gender";
    String USER_OCCUPATION = "occupation";
    String USER_DESCRIPTION = "description";
    String USER_COOKING_EXP = "cookignExperience";

    String IS_REG_FAILED = "regFailed";
    String IS_FACEBOOK_LOGIN = "isFacebookLogin";
    String IS_GOOGLE_LOGIN = "isGoogleLogin";
    String IS_USER_ACTIVATED = "isUserActivated";
    String IS_EATERY_ACTIVATED = "isEateryActivated";
    String IS_TEMP_USER = "isTempUser";
    String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    String TYPE_AUTOCOMPLETE = "/autocomplete";
    String OUT_JSON = "/json";
    String SERVER_KEY = "AIzaSyD-ilf4I_qFiduaQVwE0v9OucWBS1aUhhU";

    int TAKE_PIC_REQ_CODE = 0;
    int CHOOSE_LIB_REQ_CODE = 1;

    String OPTION_YES = "Y";
    String OPTION_NO = "N";
    String IS_EATERY_CREATED = "isEateryCreated";

    int UPLOAD_USER_PIC = 1;
    int UPLOAD_EATERY_LOGO = 2;
    int UPLOAD_EATERY_DISH = 3;

    String DISH_OFFLINE_URI = "dishURI";
    String EATERY_OBJECT = "eateryObject";
    String EATERY_DISH_LIST_OBJECT = "eateryDishListObject";

    String EATERY_IMAGES_PUBLIC = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath();
    String EATEN_IMAGES_PRIVATE = Environment.getExternalStorageDirectory().getAbsolutePath();

    int ON_BACK_PRESSED = 999;

    String EDIT_EATERY_FLAG = "editEatery";
    String ACTIVATE_EATERY_FLAG = "activateEatery";

}
