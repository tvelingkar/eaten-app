package com.app.eaten.util;

import android.util.SparseArray;

/**
 * Created by Tushar Velingkar on 4/12/15.
 */
public class EateryImagesList {

    private static final SparseArray<String> urlCache = new SparseArray<>();

    public static void reset() {
        urlCache.clear();
    }

    public static void addEntry(int key, String imgUrl) {
        urlCache.put(key, imgUrl);
    }

    public static SparseArray<String> fetchAllEntries() {
        return urlCache;
    }
}
