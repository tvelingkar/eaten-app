package com.app.eaten.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class Contacter {

	public static void sendEmail(Context ctx,String emailId,String subject,String body)
	{
		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
	            "mailto",emailId, null));
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, body);
		ctx.startActivity(Intent.createChooser(emailIntent, "Send email..."));
	}
	
}
