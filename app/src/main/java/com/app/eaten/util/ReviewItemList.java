package com.app.eaten.util;

import android.util.SparseArray;

import com.app.eaten.beans.ReviewItem;

import java.util.ArrayList;

/**
 * Created by Tushar Velingkar on 22/12/15.
 */
public class ReviewItemList {

    private static final ArrayList<ReviewItem> reviewCache = new ArrayList<>();

    public static void reset() {
        reviewCache.clear();
    }

    public static void addEntry(ReviewItem reviewObject) {
        int key = reviewCache.size();
        reviewCache.add(key, reviewObject);
    }

    public static ArrayList<ReviewItem> fetchAllEntries() {
        return reviewCache;
    }

    public static int getCount() {
        return reviewCache.size();
    }
}
