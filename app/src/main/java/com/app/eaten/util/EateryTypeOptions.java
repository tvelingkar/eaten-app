package com.app.eaten.util;

import com.app.eaten.beans.CommonTypeObject;

import java.util.ArrayList;

public class EateryTypeOptions {

    private static final ArrayList<String> cache = new ArrayList<>();
    private static final ArrayList<CommonTypeObject> typeCache = new ArrayList<>();

    public static int addEntry(CommonTypeObject value) {
        if (!cache.contains(value.name)) {
            int key = cache.size();
            cache.add(key, value.name);
            typeCache.add(key, value);
            return key;
        } else {
            return -1;
        }
    }

    public static int getKey(int position) {
        if (!typeCache.isEmpty()) {
            CommonTypeObject object = typeCache.get(position);
            if (object != null) {
                return object.id;
            }
        }
        return -1;
    }

    public static String getEntry(int position) {
        if (!cache.isEmpty()) {
            return cache.get(position);
        }
        return "";
    }

    public static void removeEntry(int key) {
        cache.remove(key);
        typeCache.remove(key);
    }

    public static int getCount() {
        return cache.size();
    }

    public static ArrayList<String> getAllData() {
        return cache;
    }
}
