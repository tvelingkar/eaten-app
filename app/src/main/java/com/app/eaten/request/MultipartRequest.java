package com.app.eaten.request;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hl0395 on 15/8/15.
 */
public class MultipartRequest<T> extends Request<T> {

    private static final String FILE_PART_NAME = "file";

    private MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
    private final Response.Listener<T> mListener;
    private final File mImageFile;
    protected Map<String, String> headers;

    public MultipartRequest(String url,
                                 ErrorListener errorListener,
                                 Listener<T> listener,
                                 File imageFile,String iName,String iPrice,String uniqueID, String eateryID) {
        super(Method.POST, url, errorListener);

        mListener = listener;
        mImageFile = imageFile;

        buildMultipartEntity(iName,iPrice,uniqueID,eateryID);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

/*
        Map<String, String> headers = super.getHeaders();

        if (headers == null
                || headers.equals(Collections.emptyMap())) {
            headers = new HashMap<String, String>();
        }

        headers.put("name", mImageFile.getName() + ".jpg");

        return headers;
*/

        Map<String, String> headers = super.getHeaders();

        if (headers == null || headers.equals(Collections.emptyMap())) {
            headers = new HashMap<String, String>();
        }
//        headers.put("Content-Type", getBodyContentType());
        headers.put("Connection", "Keep-Alive");
        headers.put("Accept", "text/plain , application/json");
        return headers;

    }


    private void buildMultipartEntity(String iName,String iPrice,String uniqueID,String eateryID) {
//        mBuilder.addBinaryBody("upload", mImageFile, ContentType.create("image/jpeg"), mImageFile.getName() + ".jpg");
         mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        //mBuilder.addBinaryBody(FILE_PART_NAME, mImageFile);

//        mBuilder.addPart("upload", new FileBody(mImageFile));
//        mBuilder.setLaxMode().setBoundary("xx");


        byte[] bFile = new byte[(int) mImageFile.length()];

        try {
            //convert file into array of bytes
            FileInputStream fileInputStream = new FileInputStream(mImageFile);
            fileInputStream.read(bFile);
            fileInputStream.close();


            System.out.println("Done");
        }catch(Exception e){
            e.printStackTrace();
        }

/*
        mBuilder.addBinaryBody("upload", bFile, ContentType.create("image/jpg"),mImageFile.getName()+".jpg");
        mBuilder.addTextBody("filename", mImageFile.getName()+".jpg");
//        mBuilder.addTextBody("filename", mImageFile.getName()+".jpg");

//        mBuilder.addTextBody("upload", mImageFile.getName() + ".jpg");
        mBuilder.addTextBody("dishName",iName);
        mBuilder.addTextBody("dishPrice ",iPrice);
        mBuilder.addTextBody("uniqueId",uniqueID);
        mBuilder.addTextBody("eateryId",eateryID);
*/

        mBuilder.addPart("upload",new FileBody(mImageFile));

            mBuilder.addPart("dishName", new StringBody(iName,ContentType.TEXT_PLAIN));
            mBuilder.addPart("dishPrice ", new StringBody(iPrice,ContentType.TEXT_PLAIN));
            mBuilder.addPart("uniqueId", new StringBody(uniqueID,ContentType.TEXT_PLAIN));
            mBuilder.addPart("eateryId", new StringBody(eateryID,ContentType.TEXT_PLAIN));


        Log.d("ID","ID ---- "+uniqueID+" --- "+eateryID);

    }

    @Override
    public String getBodyContentType() {
        String contentTypeHeader = mBuilder.build().getContentType().getValue();

        return contentTypeHeader;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mBuilder.build().writeTo(bos);


        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream bos, building the multipart request.");
        }

        return bos.toByteArray();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        T result = null;
        String jsonString = new String(response.data);
        mListener.onResponse((T) new String(response.data));
        return Response.success(result, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(T response) {
        //mListener.onResponse(response);
    }
}