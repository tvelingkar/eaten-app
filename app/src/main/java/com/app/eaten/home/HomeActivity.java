package com.app.eaten.home;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.eaten.CommonActivity;
import com.app.eaten.R;
import com.app.eaten.activities.ActivityViewProfile;
import com.app.eaten.constants.AppConstant;
import com.app.eaten.customer.SearchFragment;
import com.app.eaten.eatery.EateryFragment;
import com.app.eaten.eventbus.EventBusListener;
import com.app.eaten.login.LoginActivity;
import com.app.eaten.login.RegistrationActivity;
import com.app.eaten.selection.SelectionActivity;
import com.app.eaten.util.Contacter;
import com.app.eaten.util.UploadFileToServer;
import com.app.eaten.util.Util;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends CommonActivity {

    private Uri imageURI;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private Intent intent;
    private String fileName;
    private ImageView imageView;
    private File file = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_acttivity);

        setupActionbar();

        openSelection();

        registerForEventBus();

    }

    @Override
    public void onBackPressed() {
        int count = fragmentManager.getBackStackEntryCount();
        Log.v("Eaten", "Back Pressed Count "+count);
        if (count == 0) {
            super.onBackPressed();
            finish();
        } else {
            fragmentManager.popBackStackImmediate();
        }
    }

    public void onEvent(EventBusListener mEventListener) {
        switch(mEventListener.getType()) {
            case ACTIVATE_EATERY_FLAG :
                setupNavMenu();
                break;
        }
    }

    private void registerForEventBus() {

        try {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);

        } catch (ClassCastException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    private void setupActionbar() {

        // Initializing Toolbar and setting it as the actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.navView);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }

                drawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {

                    case R.id.menu_profile:
                        intent = new Intent(HomeActivity.this, ActivityViewProfile.class);
                        startActivity(intent);
                        return true;
                    case R.id.menu_rate:
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                        return true;
                    case R.id.menu_share:
                        Intent localIntent = new Intent("android.intent.action.SEND");
                        localIntent.setType("text/plain");
                        localIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                                "Eaten APP - Delicious Home made food!\n" + "Download Now "
                                        + "http://eatenApp.com/");
                        Intent chooserIntent = Intent.createChooser(localIntent, "Select app to share");
                        startActivity(chooserIntent);
                        return true;
                    case R.id.menu_feedback:
                        Contacter.sendEmail(HomeActivity.this, "contact@eatenapp.com",
                                getString(R.string.feedback_mail_subject), "");
                        return true;
                    case R.id.menu_edit_profile:
                        intent = new Intent(HomeActivity.this, RegistrationActivity.class);
                        startActivityForResult(intent, EDIT_PROFILE_REQ);
                        return true;
                    case R.id.menu_edit_eatery:
                        EventBusListener eventBusListener = new EventBusListener();
                        eventBusListener.setType(EDIT_EATERY_FLAG);
                        EventBus.getDefault().post(eventBusListener);
                        return true;
                    case R.id.menu_change_password:
                        Toast.makeText(getApplicationContext(), "Password Selected", Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                        Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                        return true;

                }
            }
        });
        if (Util.getPrefs(HomeActivity.this).getBoolean(LOGIN_STATUS, false)) {
            setupProfileNav();
            setupNavMenu();
        }

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("Eaten", "Request Code "+requestCode);
        switch (requestCode) {
            case REG_REQ:
                if (resultCode == Activity.RESULT_OK) {
                    Log.v("Eaten", "Your profile was registered Successfully!!!");
                    Toast.makeText(HomeActivity.this, "Your profile was registered Successfully!!!", Toast.LENGTH_SHORT).show();
                    Util.getPrefs(this).edit().putBoolean(IS_FIRST_TIME, false).apply();
                    Util.getPrefs(this).edit().putBoolean(IS_REG_FAILED, false).apply();
                    openSelection();
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Log.v("Eaten", "Some error occured while registering. Please try again later!!!");
                    Toast.makeText(HomeActivity.this, "Some error occured while registering. Please try again later!!!", Toast.LENGTH_SHORT).show();
                    Util.getPrefs(this).edit().putBoolean(IS_REG_FAILED, true).apply();
                    openSelection();
                } else if (resultCode == ON_BACK_PRESSED) {
                    finish();
                }
                break;
            case EDIT_PROFILE_REQ:
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(HomeActivity.this, "Your profile was updated Successfully!!!", Toast.LENGTH_SHORT).show();
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(HomeActivity.this, "Some error occured while updating. Please try again later!!!", Toast.LENGTH_SHORT).show();
                }
                break;

            case SELECTION_REQ:
                if (resultCode == ON_BACK_PRESSED) {
                    finish();
                } else {
                    openSelection();
                }
                break;

            case LOGIN_REQ:
                if (resultCode == ON_BACK_PRESSED) {
                    finish();
                } else {
                    setupProfileNav();
                    setupNavMenu();
                    openSelection();
                }
                break;
        }
    }

    private void openSelection() {
        if (Util.getPrefs(this).getInt(SELECTION_VALUE, 0) == 0) {
            intent = new Intent(this, SelectionActivity.class);
            startActivityForResult(intent, SELECTION_REQ);
        } else {
            if (!Util.getPrefs(HomeActivity.this).getBoolean(LOGIN_STATUS, false)) {
                intent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivityForResult(intent, LOGIN_REQ);
            } else {
                if (Util.getPrefs(this).getBoolean(IS_FIRST_TIME, true) || Util.getPrefs(this).getBoolean(IS_REG_FAILED, false)) {
                    intent = new Intent(HomeActivity.this, RegistrationActivity.class);
                    startActivityForResult(intent, REG_REQ);
                } else {
                    if (!Util.getPrefs(this).getBoolean(IS_GOOGLE_LOGIN, false) && !Util.getPrefs(this).getBoolean(IS_FACEBOOK_LOGIN, false)) {
                        if (Util.getPrefs(this).getBoolean(IS_USER_ACTIVATED, false)) {
                            activateUser();
                        }
                    } else {
                        if (Util.getPrefs(HomeActivity.this).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
                            addFragment(new EateryFragment(), false, false);
                        } else if (Util.getPrefs(HomeActivity.this).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == CUSTOMER_SELECTION) {
                            addFragment(new SearchFragment(), false, false);
                        }
                    }
                }
            }
        }
    }

    private void activateUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this)
                .setTitle("Enter Activation Code");
        final FrameLayout frameView = new FrameLayout(HomeActivity.this);
        builder.setView(frameView);

        final AlertDialog alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.activate_user, frameView);

        final EditText activationCode = (EditText) dialoglayout.findViewById(R.id.txtCode);
        final TextView textView = (TextView) dialoglayout.findViewById(R.id.errorMsg);
        Button activateButton = (Button) dialoglayout.findViewById(R.id.btnActivate);

        activateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (activationCode.getText() == null || activationCode.getText().toString().isEmpty()) {
                    textView.setText("Please Enter Activation Code");
                } else {
                    textView.setText("");
                    JSONObject loginRequestObj = new JSONObject();
                    JSONObject data = new JSONObject();

                    try {
                        data.put("uniqueId", Util.getPrefs(HomeActivity.this).getString(LOGIN_UNIQUE_ID, ""));
                        data.put("code", activationCode.getText());
                        loginRequestObj.putOpt("user", data);
                        Log.v("Eaten", "Request -- " + loginRequestObj.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JsonObjectRequest loginRequest = new JsonObjectRequest(Request.Method.POST, SERVER_BASE_URL + VERIFY_USER_URL, loginRequestObj, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            alertDialog.dismiss();
                            Util.getPrefs(HomeActivity.this).edit().putBoolean(IS_USER_ACTIVATED, true).commit();
                            openSelection();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(HomeActivity.this, "Activation Code did not match", Toast.LENGTH_SHORT).show();
                            openSelection();
                        }
                    }) {
                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8";
                        }
                    };

                    addRequestToQueue(loginRequest);
                }
            }
        });

        alertDialog.show();
    }

    private void setupProfileNav() {
        View nav_header = LayoutInflater.from(this).inflate(R.layout.nav_header, navigationView, false);
        if (!"".equals(Util.getPrefs(this).getString(USER_PROFILE_URI, ""))) {
            Picasso.with(this).load(Util.getPrefs(this).getString(USER_PROFILE_URI, ""))
                    .placeholder(ContextCompat.getDrawable(this, R.drawable.add_photo))
                    .fit()
                    .into(((CircleImageView) nav_header.findViewById(R.id.profile_image)));
        }
        ((TextView) nav_header.findViewById(R.id.username)).setText(String.format("%s %s", Util.getPrefs(this).getString(USER_FNAME, ""), Util.getPrefs(this).getString(USER_LNAME, "")));
        ((TextView) nav_header.findViewById(R.id.email)).setText(Util.getPrefs(this).getString(USER_EMAIL, ""));
        navigationView.addHeaderView(nav_header);
    }

    private void setupNavMenu(){
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.getMenu().clear();
        if (Util.getPrefs(this).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == CUSTOMER_SELECTION) {
            navigationView.inflateMenu(R.menu.menu_customer_main);
        } else if (Util.getPrefs(this).getInt(SELECTION_VALUE, CUSTOMER_SELECTION) == EATERY_SELECTION) {
            navigationView.inflateMenu(R.menu.menu_eatery_main);
            if (!Util.getPrefs(HomeActivity.this).getBoolean(IS_EATERY_ACTIVATED, false)) {
                Menu menu = navigationView.getMenu();
                menu.findItem(R.id.menu_edit_eatery).setVisible(false);
            }
        }
    }
}
