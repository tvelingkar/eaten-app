package com.app.eaten.adapter;

import android.view.View;

/**
 * Created by hl0395 on 15/8/15.
 */
public interface PopulationListener<T> {

    public void populateFrom(View v, int position, final T row, View[] views);

    // Call when the row is created first time.
    public void onRowCreate(View[] views);

}