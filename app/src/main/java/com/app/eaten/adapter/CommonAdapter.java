package com.app.eaten.adapter;

/**
 * Created by hl0395 on 15/8/15.
 */

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;

import java.util.List;

public class CommonAdapter<T> extends BaseAdapter {

    private List<T> elements;
    private Context mContext;

    private int layoutID = 0;
    private int[] viewIds;
    private int lastSelectedPosition;
    private int selectedRowBGColor = -1;
    private int unSelectedRowBGColor = Color.parseColor("#00000000");
    private int selectedRowBGResource = -1;
    private int unSelectedRowBGResource = -1;
    private int oddRowColor = -1;
    private int evenRowColor = -1;
    private Animation rowAnimation;

    private PopulationListener<T> listener;

    public CommonAdapter(Context context, final List<T> elem) {
        this.mContext = context;
        this.elements = elem;
    }

    public void setLayoutTextViews(int layoutID, int[] viewIDs) {
        this.layoutID = layoutID;
        this.viewIds = viewIDs;
    }

    public void clear() {
        elements.clear();
    }

    public void setListAnimation(Animation anim) {
        this.rowAnimation = anim;
    }

    public void add(final T row) {
        elements.add(row);
    }

    public void add(int position, final T row) {
        elements.add(position, row);
    }

    public void addAll(final List<T> rows) {
        elements.addAll(rows);
    }

    public void set(final List<T> rows) {
        elements = rows;
    }

    public void remove(int position) {
        elements.remove(position);
    }

    public int getCount() {
        return elements.size();
    }

    public T getItem(int position) {
        if (getCount() > 0)
            return elements.get(position);
        return null;
    }

    public List<T> getItems() {
        return elements;
    }

    public long getItemId(int position) {
        return position;
    }

    public void setPopulationListener(PopulationListener<T> listener) {
        this.listener = listener;
    }

    public View getView(int position, View row, ViewGroup parent) {
        RowHolder holder = null;

        if (row == null) {

            row = (View) LayoutInflater.from(mContext).inflate(layoutID,
                    parent, false);

            holder = new RowHolder(row, viewIds);
            listener.onRowCreate(holder.getViews());
            row.setTag(holder);

        } else {
            holder = (RowHolder) row.getTag();
        }

        if (row != null) {

            // Set alternative row color
            if (oddRowColor != -1) {
                if (position % 2 == 1) {
                    row.setBackgroundColor(oddRowColor);
                } else {
                    row.setBackgroundColor(evenRowColor);
                }
            }

            // Selected row
            if (selectedRowBGColor != -1) {
                if (lastSelectedPosition == position) {
                    // Selected Row Color
                    row.setBackgroundColor(selectedRowBGColor);
                } else {
                    // UnSelected Row Color
                    row.setBackgroundColor(unSelectedRowBGColor);
                }
            } else if (selectedRowBGResource != -1) {
                if (lastSelectedPosition == position) {
                    // Selected Row Resource
                    row.setBackgroundResource(selectedRowBGResource);
                } else {
                    // UnSelected Row Resource
                    row.setBackgroundResource(unSelectedRowBGResource);
                }
            }

        }

        /**
         * Here we store the position value with key as layoutID to handle
         * alternate colors for list rows.
         *
         * please refer OnItemClickListener (onListClick in watch list) for
         * listView.
         *
         */
        row.setTag(layoutID, position);

//		if (rowAnimation != null)
//			row.startAnimation(rowAnimation);

        listener.populateFrom(row, position, elements.get(position),
                holder.getViews());

        return (row);
    }

    public void setLastSelectedPosition(int position) {
        lastSelectedPosition = position;
    }

    public void setRowBackgroundColor(int selectedRowBGColor,
                                      int unSelectedRowBGColor) {
        this.selectedRowBGColor = selectedRowBGColor;
        this.unSelectedRowBGColor = unSelectedRowBGColor;
    }

    public void setRowBackgroundResource(int selectedRowBGResource,
                                         int unSelectedRowBGResource) {
        this.selectedRowBGResource = selectedRowBGResource;
        this.unSelectedRowBGResource = unSelectedRowBGResource;
    }

    public void setAlternativeRowColor(int oddRowColor, int evenRowColor) {
        this.oddRowColor = oddRowColor;
        this.evenRowColor = evenRowColor;
    }

    public void swapItems(List<T> newData) {
        elements = newData;
        notifyDataSetChanged();
    }

}