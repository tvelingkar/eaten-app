package com.app.eaten.adapter;

/**
 * Created by Vivek CA on 12-07-2015.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class FragmentPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<String> pageTitles = new ArrayList<String>();
    private ArrayList<Fragment> fragmentList = new ArrayList<Fragment>();

    public FragmentPagerAdapter(Fragment fragment) {
        super(fragment.getFragmentManager());
    }

    /**
     * Constructor of the class
     */
    public FragmentPagerAdapter(FragmentManager fm,
                                ArrayList<Fragment> fragmentList, ArrayList<String> pageTitles) {
        super(fm);
        this.fragmentList = fragmentList;
        this.pageTitles = pageTitles;
    }


    /**
     * This method will be invoked when a page is requested to create
     */
    @Override
    public Fragment getItem(int index) {
        return fragmentList.get(index);
    }

    /**
     * Returns the number of pages
     */
    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (pageTitles != null)
            return pageTitles.get(position);
        else
            return "";

    }

}
